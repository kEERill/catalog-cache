<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('brand_id')->unique();

            $table->string('name');
            $table->string('code');
            $table->text('description')->nullable();
            $table->string('logo_file')->nullable();
            $table->string('logo_url')->nullable();

            $table->timestamps(6);
        });

        Schema::create('categories', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('category_id')->unique();

            $table->string('name');
            $table->string('code');
            $table->unsignedBigInteger('parent_id')->nullable();

            $table->timestamps(6);
        });

        Schema::create('images', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('image_id')->unique();

            $table->unsignedBigInteger('product_id');
            $table->string('name')->nullable();
            $table->integer('sort');
            $table->string('file')->nullable();
            $table->string('url')->nullable();

            $table->timestamps(6);
        });

        Schema::create('products', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_id')->unique();

            $table->string('main_image')->nullable();
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->string('name');
            $table->string('code');
            $table->text('description')->nullable();
            $table->integer('type');
            $table->string('vendor_code')->nullable();
            $table->string('barcode')->nullable();
            $table->decimal('weight', 18, 4)->nullable();
            $table->decimal('weight_gross', 18, 4)->nullable();
            $table->decimal('width', 18, 4)->nullable();
            $table->decimal('height', 18, 4)->nullable();
            $table->decimal('length', 18, 4)->nullable();
            $table->boolean('is_adult')->nullable();

            $table->timestamps(6);
        });

        Schema::create('offers', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('offer_id')->unique();

            $table->unsignedBigInteger('product_id');
            $table->bigInteger('base_price')->nullable();

            $table->timestamps(6);
        });

        Schema::create('properties', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('property_id')->unique();

            $table->string('name', 500);
            $table->string('code');
            $table->string('type', 20);

            $table->timestamps(6);
        });

        Schema::create('property_directory_values', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('directory_value_id')->unique();

            $table->unsignedBigInteger('property_id');
            $table->string('name', 1000)->nullable();
            $table->string('code')->nullable();
            $table->string('value', 1000);
            $table->string('type', 20);

            $table->timestamps(6);
        });

        Schema::create('product_property_values', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_property_value_id')->unique();

            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('directory_value_id')->nullable();
            $table->string('type', 20)->nullable();
            $table->string('value', 1000)->nullable();
            $table->string('name', 1000)->nullable();

            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_property_values');
        Schema::dropIfExists('property_directory_values');
        Schema::dropIfExists('properties');
        Schema::dropIfExists('offers');
        Schema::dropIfExists('products');
        Schema::dropIfExists('images');
        Schema::dropIfExists('categories');
        Schema::dropIfExists('brands');
    }
};
