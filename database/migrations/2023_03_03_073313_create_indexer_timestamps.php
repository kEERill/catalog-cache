<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up()
    {
        Schema::create('indexer_timestamps', function (Blueprint $table) {
            $table->id();

            $table->string('index');

            $table->string('stage');
            $table->string('index_hash');

            $table->timestamp('last_schedule', 6);
        });
    }

    public function down()
    {
        Schema::dropIfExists('indexer_timestamps');
    }
};
