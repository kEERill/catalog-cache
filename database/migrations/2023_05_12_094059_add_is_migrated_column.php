<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('nameplates', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('nameplate_product', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('brands', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('categories', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('actual_category_properties', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('properties', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('property_directory_values', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('products', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('images', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('product_property_values', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('offers', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('product_groups', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });

        Schema::table('product_group_product', function (Blueprint  $table) {
            $table->boolean('is_migrated')->default(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('nameplates', function (Blueprint $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('nameplate_product', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('brands', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('categories', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('actual_category_properties', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('properties', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('property_directory_values', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('products', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('images', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('product_property_values', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('offers', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('product_groups', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });

        Schema::table('product_group_product', function (Blueprint  $table) {
            $table->dropColumn(['is_migrated']);
        });
    }
};
