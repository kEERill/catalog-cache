<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('product_groups', 'activity')) {
            Schema::table('product_groups', function (Blueprint  $table) {
                $table->renameColumn('activity', 'is_active');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasColumn('product_groups', 'is_active')) {
            Schema::table('product_groups', function (Blueprint  $table) {
                $table->renameColumn('is_active', 'activity');
            });
        }
    }
};
