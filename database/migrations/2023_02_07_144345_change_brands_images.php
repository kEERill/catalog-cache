<?php

use Illuminate\Database\Migrations\Migration;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        DB::table('brands')->where('logo_file', 'LIKE', '%"path":%')
            ->chunkById(100, function ($brands) {
                foreach ($brands as $brand) {
                    $fileData = json_decode($brand->logo_file, true);

                    if (isset($fileData['path'])) {
                        DB::table('brands')->where('id', $brand->id)->update([
                            'logo_file' => $fileData['path'],
                        ]);
                    }
                }
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        //Не откатываем
    }
};
