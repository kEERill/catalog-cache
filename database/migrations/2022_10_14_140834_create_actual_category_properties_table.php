<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('actual_category_properties', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('actual_category_property_id')->unique();

            $table->unsignedBigInteger('property_id');
            $table->unsignedBigInteger('category_id');

            $table->boolean('is_gluing');

            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('actual_category_properties');
    }
};
