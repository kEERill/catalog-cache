<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_groups', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_group_id')->unique();

            $table->string('name');
            $table->unsignedBigInteger('category_id');
            $table->unsignedBigInteger('main_product_id')->nullable();

            $table->timestamps(6);
        });

        Schema::create('product_group_product', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('product_group_product_id')->unique();

            $table->unsignedBigInteger('product_group_id');
            $table->unsignedBigInteger('product_id');

            $table->timestamps(6);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_group_product');
        Schema::dropIfExists('product_groups');
    }
};
