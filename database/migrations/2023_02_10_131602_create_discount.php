<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('offer_id');
            $table->unsignedTinyInteger('value_type');
            $table->unsignedInteger('value');
            $table->timestamps(6);

            $table->foreign('offer_id')
                ->references('offer_id')
                ->on('offers')
                ->onDelete('cascade');
        });

        Schema::table('offers', function (Blueprint $table) {
            $table->unsignedInteger('price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('offers', function (Blueprint $table) {
            $table->dropColumn('price');
        });

        Schema::dropIfExists('discounts');
    }
};
