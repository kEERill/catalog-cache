<?php

namespace Tests\Factories;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Elastic\Data\ElasticModel;
use Ensi\TestFactories\Factory;
use Faker\Generator;
use Illuminate\Support\Collection;

abstract class BaseElasticFactory extends Factory
{
    protected ElasticIndex $index;
    protected int $count = 1;

    public function __construct(Generator $faker)
    {
        parent::__construct($faker);
        $this->index = $this->index();
    }

    abstract protected function index(): ElasticIndex;

    abstract public function make(array $extra = []): ElasticModel;

    public function count(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    public function createOne(array $extra = []): ElasticModel
    {
        $this->index->createIfNotExist();

        $data = $this->make($extra);
        $this->index->bulk([
            ["index" => ["_id" => $data->getId()]],
            $data->toJson(),
        ]);

        return $data;
    }

    public function create(array $extra = []): Collection|ElasticModel
    {
        $this->index->createIfNotExist();

        $items = collect();
        $bulk = [];
        foreach (range(1, $this->count) as $i) {
            $data = $this->make($extra);

            $bulk[] = ["index" => ["_id" => $data->getId()]];
            $bulk[] = $data->toJson();

            $items->push($data);
        }

        $this->index->bulk($bulk);
        // Это нужно, чтобы эластик сразу данные начинал отдавать в поиске, иначе у него внутренние задержки ломают тесты
        $this->index->indicesRefresh();

        if ($items->count() == 1) {
            return $items->first();
        } else {
            return $items;
        }
    }
}
