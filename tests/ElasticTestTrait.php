<?php

namespace Tests;

use App\Domain\Elastic\Concerns\ElasticIndex;
use Ensi\LaravelElasticQuery\ElasticClient;
use Illuminate\Support\Facades\ParallelTesting;

trait ElasticTestTrait
{
    protected function setUpElasticTestTrait(): void
    {
        config()->set('app.stage', config('app.stage') . ParallelTesting::token() ?: 0);
        /** @var ElasticClient $elastic */
        $elastic = resolve(ElasticClient::class);
        $indices = $elastic->catIndices(ElasticIndex::indexNamePrefix());
        foreach ($indices as $index) {
            $elastic->indicesDelete($index['index']);
        }
    }
}
