<?php

namespace Tests;

use Ensi\MarketingClient\Api\CalculatorsApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== Internal Services ===============

    // =============== Marketing ===============
    public function mockMarketingCalculatorsApi(): MockInterface|CalculatorsApi
    {
        return $this->mock(CalculatorsApi::class);
    }
}
