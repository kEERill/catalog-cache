<?php

use App\Domain\Kafka\Actions\Listen\ListenActualCategoryPropertyAction;
use App\Domain\Kafka\Actions\Listen\ListenBrandAction;
use App\Domain\Kafka\Actions\Listen\ListenCategoryAction;
use App\Domain\Kafka\Actions\Listen\ListenDiscountCatalogCalculateAction;
use App\Domain\Kafka\Actions\Listen\ListenNameplateAction;
use App\Domain\Kafka\Actions\Listen\ListenNameplateProductAction;
use App\Domain\Kafka\Actions\Listen\ListenOfferAction;
use App\Domain\Kafka\Actions\Listen\ListenProductGroupAction;
use App\Domain\Kafka\Actions\Listen\ListenProductGroupProductAction;
use App\Domain\Kafka\Actions\Listen\ListenPropertyAction;
use App\Domain\Kafka\Actions\Listen\ListenPropertyDirectoryValueAction;
use App\Domain\Kafka\Actions\Listen\ListenPublishedImageAction;
use App\Domain\Kafka\Actions\Listen\ListenPublishedProductAction;
use App\Domain\Kafka\Actions\Listen\ListenPublishedPropertyValueAction;
use Ensi\LaravelInitialEventPropagation\RdKafkaConsumerMiddleware;

return [
    /*
    | Optional, defaults to empty array.
    | Array of global middleware fully qualified class names.
    */
    'global_middleware' => [ RdKafkaConsumerMiddleware::class ],
    'stop_signals' => [SIGTERM, SIGINT],

    'processors' => [
        [
            'topic' => 'offers',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenOfferAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'brands',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenBrandAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'categories',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenCategoryAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'property-directory-values',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenPropertyDirectoryValueAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'properties',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenPropertyAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'actual-category-properties',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenActualCategoryPropertyAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'published-images',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenPublishedImageAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'published-products',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenPublishedProductAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'published-property-values',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenPublishedPropertyValueAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'product-groups',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenProductGroupAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'product-group-products',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenProductGroupProductAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'nameplates',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenNameplateAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'nameplate-product-links',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenNameplateProductAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'discount-catalog-calculate',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenDiscountCatalogCalculateAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
    ],

    'consumer_options' => [
       /** options for consumer with name `default` */
       'default' => [
          /*
          | Optional, defaults to 20000.
          | Kafka consume timeout in milliseconds.
          */
         'consume_timeout' => 20000,

          /*
          | Optional, defaults to empty array.
          | Array of middleware fully qualified class names for this specific consumer.
          */
         'middleware' => [],
       ],
    ],
 ];
