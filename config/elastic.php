<?php

use App\Domain\Offers\Actions\DispatchOffersToIndexingAction;
use App\Domain\Offers\Elastic\OfferIndex;

return [
    'indexes' => [
        OfferIndex::class => [
            'scan' => DispatchOffersToIndexingAction::class,
        ],
    ],
];
