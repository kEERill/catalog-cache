<?php

namespace App\Domain\Discounts\Actions;

use App\Domain\Offers\Actions\UpdateOffersAction;
use App\Domain\Offers\Models\Offer;
use Ensi\MarketingClient\Api\CalculatorsApi;
use Ensi\MarketingClient\Dto\CalculateCatalogOffer;
use Ensi\MarketingClient\Dto\CalculateCatalogRequest;
use Ensi\MarketingClient\Dto\CalculateCatalogResponseData;
use Ensi\MarketingClient\Dto\CalculatedDiscount;
use Illuminate\Database\Eloquent\Collection as DatabaseCollection;
use Illuminate\Support\Collection;
use Psr\Log\LoggerInterface;
use Throwable;

class CalculateDiscountsAction
{
    protected LoggerInterface $logger;
    protected Collection $calculatedOffersData;

    public function __construct(
        protected readonly CalculatorsApi $calculatorsApi,
        protected readonly UpdateOffersAction $updateOffersAction,
    ) {
        $this->logger = logger()->channel('offers:discounts');
    }

    public function execute(DatabaseCollection $offers): void
    {
        $offers = $offers->keyBy('offer_id');

        $data = $this->calculateDiscount($offers);
        if (is_null($data)) {
            return;
        }
        $this->calculatedOffersData = collect($data->getOffers())->keyBy('offer_id');

        $this->saveDiscountData($offers);
        $offers->load('discount');
    }

    protected function calculateDiscount(DatabaseCollection $offers): ?CalculateCatalogResponseData
    {
        $requestOffers = [];

        /** @var Offer $offer */
        foreach ($offers as $offer) {
            if ($offer->base_price && !$offer->price) {
                $requestOffer = new CalculateCatalogOffer();
                $requestOffer->setOfferId($offer->offer_id);
                $requestOffer->setProductId($offer->product_id);
                $requestOffer->setCost($offer->base_price);
                $requestOffers[] = $requestOffer;
            }
        }

        if (blank($requestOffers)) {
            return null;
        }

        $this->logger->info(
            "Загрузка скидок по офферам",
            ['offer_ids' => collect($requestOffers)->pluck('offer_id')->all()]
        );

        $request = new CalculateCatalogRequest();
        $request->setOffers($requestOffers);

        return $this->calculatorsApi->calculateCatalog($request)->getData();
    }

    protected function saveDiscountData(DatabaseCollection $offers): void
    {
        $this->updateOffersAction->execute(
            offers: $offers,
            fillModel: $this->fillDiscount(...),
            error: function (Offer $offer, Throwable $e) {
                $this->logger->error("Ошибка обновление скидки у оффера {$offer->offer_id}", ['message' => $e->getMessage()]);
            }
        );
    }

    protected function fillDiscount(Offer $offer): void
    {
        $calculatedOffer = $this->calculatedOffersData->get($offer->offer_id);

        if (is_null($calculatedOffer)) {
            return;
        }
        $this->logger->info("Обновление скидки у оффера {$offer->offer_id}", $calculatedOffer->toArray());

        $offer->price = $calculatedOffer->getPrice();

        /** @var CalculatedDiscount|null $discount */
        $discount = $calculatedOffer->getDiscounts() ? current($calculatedOffer->getDiscounts()) : null;
        if ($discount) {
            $offer->discount()->create($discount->toArray());
        }

        $offer->notMark();
    }
}
