<?php

namespace App\Domain\Discounts\Models;

use App\Domain\Discounts\Models\Tests\Factories\DiscountFactory;
use App\Domain\Offers\Models\Offer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property int $id
 *
 * @property int $value_type - тип значения
 * @property int $value - размер скидки
 *
 * @property Offer $offer - оффер
 */
class Discount extends Model
{
    protected $table = 'discounts';

    protected $fillable = ['value', 'value_type'];

    public function offer(): BelongsTo
    {
        return $this->belongsTo(Offer::class, 'offer_id', 'offer_id');
    }

    public static function factory(): DiscountFactory
    {
        return DiscountFactory::new();
    }
}
