<?php

namespace App\Domain\Discounts\Models\Tests\Factories;

use App\Domain\Discounts\Models\Discount;
use Ensi\MarketingClient\Dto\DiscountValueTypeEnum;
use Illuminate\Database\Eloquent\Factories\Factory;

class DiscountFactory extends Factory
{
    protected $model = Discount::class;

    public function definition(): array
    {
        return [
            'value_type' => $this->faker->randomElement(DiscountValueTypeEnum::getAllowableEnumValues()),
            'value' => $this->faker->randomNumber(),
        ];
    }
}
