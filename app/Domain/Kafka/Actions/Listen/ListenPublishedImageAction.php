<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedImage\PublishedImageEventMessage;
use App\Domain\Offers\Models\Image;
use RdKafka\Message;

class ListenPublishedImageAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = PublishedImageEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'product_id',
                'name',
                'sort',
                'file',
            ],
            findModel: fn () => Image::query()->where('image_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new Image();
                $model->image_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel:  function (Image $model) use ($modelPayload) {
                $model->product_id = $modelPayload->product_id;
                $model->name = $modelPayload->name;
                $model->sort = $modelPayload->sort;
                $model->file = $modelPayload->file;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
