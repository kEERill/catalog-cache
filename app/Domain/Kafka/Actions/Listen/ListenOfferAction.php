<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Offer\OfferEventMessage;
use App\Domain\Offers\Actions\UpdateOfferAction;
use App\Domain\Offers\Models\Offer;
use RdKafka\Message;

class ListenOfferAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
        protected UpdateOfferAction $updateOfferAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = OfferEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: ['base_price'],
            findModel: fn () => Offer::query()->where('offer_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new Offer();
                $model->offer_id = $modelPayload->id;
                $model->product_id = $modelPayload->product_id;

                return $model;
            },
            fillModel:  function (Offer $model) use ($modelPayload) {
                $this->updateOfferAction->execute(
                    offer: $model,
                    fillModel: function (Offer $model) use ($modelPayload) {
                        $model->base_price = $modelPayload->base_price;
                    },
                    save: false
                );
            },
        );
    }
}
