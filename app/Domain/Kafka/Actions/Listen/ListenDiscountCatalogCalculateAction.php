<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Discount\DiscountCatalogCalculateMessage;
use App\Domain\Offers\Actions\UpdateOffersAction;
use App\Domain\Offers\Models\Offer;
use Illuminate\Database\Eloquent\Collection;
use RdKafka\Message;

class ListenDiscountCatalogCalculateAction
{
    public function __construct(protected UpdateOffersAction $updateOffersAction)
    {
    }

    public function execute(Message $message): void
    {
        $eventMessage = DiscountCatalogCalculateMessage::makeFromRdKafka($message);
        /** @var Collection<Offer> $offers */
        $offers = Offer::query()
            ->with('discount')
            ->whereIn('product_id', $eventMessage->productIds)
            ->whereNotNull('price')
            ->get();

        $this->updateOffersAction->execute(
            offers: $offers,
            fillModel: function (Offer $offer) {
                $offer->price = null;
            }
        );
    }
}
