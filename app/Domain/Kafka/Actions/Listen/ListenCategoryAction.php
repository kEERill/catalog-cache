<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Category\CategoryEventMessage;
use App\Domain\Offers\Models\Category;
use RdKafka\Message;

class ListenCategoryAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = CategoryEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'name',
                'code',
                'parent_id',
            ],
            findModel: fn () => Category::query()->where('category_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new Category();
                $model->category_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel:  function (Category $model) use ($modelPayload) {
                $model->name = $modelPayload->name;
                $model->code = $modelPayload->code;
                $model->parent_id = $modelPayload->parent_id;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
