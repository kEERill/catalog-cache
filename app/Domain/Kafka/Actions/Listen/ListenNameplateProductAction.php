<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\NameplateProduct\NameplateProductEventMessage;
use App\Domain\Offers\Models\NameplateProduct;
use RdKafka\Message;

class ListenNameplateProductAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = NameplateProductEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'nameplate_id',
                'product_id',
            ],
            findModel: fn () => NameplateProduct::query()->where('nameplate_product_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new NameplateProduct();
                $model->nameplate_product_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel:  function (NameplateProduct $model) use ($modelPayload) {
                $model->nameplate_id = $modelPayload->nameplate_id;
                $model->product_id = $modelPayload->product_id;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
