<?php

namespace App\Domain\Kafka\Actions\Listen;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroup\ProductGroupEventMessage;
use App\Domain\Offers\Models\ProductGroup;
use RdKafka\Message;

class ListenProductGroupAction
{
    public function __construct(
        protected SyncModelAction $syncModelAction,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = ProductGroupEventMessage::makeFromRdKafka($message);
        $modelPayload = $eventMessage->attributes;

        $this->syncModelAction->execute(
            eventMessage: $eventMessage,
            interestingUpdatedFields: [
                'category_id',
                'name',
                'main_product_id',
                'is_active',
            ],
            findModel: fn () => ProductGroup::query()->where('product_group_id', $modelPayload->id)->first(),
            createModel: function () use ($modelPayload) {
                $model = new ProductGroup();
                $model->product_group_id = $modelPayload->id;
                $model->created_at = $modelPayload->created_at;

                return $model;
            },
            fillModel:  function (ProductGroup $model) use ($modelPayload) {
                $model->category_id = $modelPayload->category_id;
                $model->name = $modelPayload->name;
                $model->main_product_id = $modelPayload->main_product_id;
                $model->is_active = $modelPayload->is_active;
                $model->updated_at = $modelPayload->updated_at;
            },
        );
    }
}
