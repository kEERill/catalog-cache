<?php

use App\Domain\Kafka\Actions\Listen\ListenPropertyAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Property\PropertyEventMessage;
use App\Domain\Offers\Models\Property;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-property');

test("Action ListenPropertyAction create success", function () {
    /** @var IntegrationTestCase $this */
    $propertyId = 1;
    $message = PropertyEventMessage::factory()->attributes(['id' => $propertyId])->event(ModelEventMessage::CREATE)->make();

    // Создаём оффер который связан с этим свойством и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::createFromProperty($propertyId);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::createFromProperty($propertyId + 1);

    resolve(ListenPropertyAction::class)->execute($message);

    assertDatabaseHas((new Property())->getTable(), [
        'property_id' => $propertyId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
});

test("Action ListenPropertyAction update success", function (array $dirty, bool $isDirty) {
    /** @var IntegrationTestCase $this */
    /** @var Property $property */
    $property = Property::factory()->create();
    $updatedAt = $property->updated_at->addDay();
    $message = PropertyEventMessage::factory()
        ->attributes(['id' => $property->property_id, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём оффер который связан с этим свойством и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::createFromProperty($property->property_id);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::createFromProperty($property->property_id + 1);

    resolve(ListenPropertyAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $property->updated_at;
    $property->refresh();
    assertEquals($needUpdatedAt, $property->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with([
    [['display_name'], true],
    [['name'], false],
]);

test("Action ListenPropertyAction delete success", function () {
    /** @var IntegrationTestCase $this */
    /** @var Property $property */
    $property = Property::factory()->create();
    $message = PropertyEventMessage::factory()
        ->attributes(['id' => $property->property_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём оффер, который связан с этим свойством и не должен пометиться на индексацию
    // т.к. при удалении не помечаем
    $offer = OfferLinkedFactory::createFromProperty($property->property_id);

    resolve(ListenPropertyAction::class)->execute($message);

    assertDatabaseMissing((new Property())->getTable(), ['property_id' => $property->property_id]);
    // Удаление произошло, но пометки нет
    assertNewModelFieldEquals($offer, 'updated_at');
});
