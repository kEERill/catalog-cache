<?php

use App\Domain\Discounts\Models\Discount;
use App\Domain\Kafka\Actions\Listen\ListenDiscountCatalogCalculateAction;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Discount\DiscountCatalogCalculateMessage;
use App\Domain\Offers\Models\Offer;
use Ensi\TestFactories\FakerProvider;
use Illuminate\Database\Eloquent\Collection;

use Illuminate\Database\Eloquent\Model;

use function Pest\Laravel\assertDatabaseHas;

use function Pest\Laravel\assertDatabaseMissing;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-discount-catalog-calculate');

test("Action ListenDiscountCatalogCalculateAction success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $initialEvent = Event::getFacadeRoot();
    Event::fake();
    Model::setEventDispatcher($initialEvent);

    /** @var Collection<Offer> $offers */
    $offers = Offer::factory()->count(5)
        ->sequence(
            ['price' => null],
            ['price' => 10_00],
        )
        ->create();

    /** @var Collection<Discount> $discounts */
    $discounts = collect();
    foreach ($offers as $offer) {
        if ($offer->price) {
            $discounts->add(Discount::factory()->for($offer)->create());
        }
    }

    $message = DiscountCatalogCalculateMessage::factory()->make([
        'product_ids' => $offers->pluck('product_id')->all(),
    ]);

    resolve(ListenDiscountCatalogCalculateAction::class)->execute($message);

    foreach ($offers as $offer) {
        assertDatabaseHas((new Offer())->getTable(), ['id' => $offer->id, 'price' => null]);
    }
    foreach ($discounts as $discount) {
        assertDatabaseMissing((new Discount())->getTable(), ['id' => $discount->id]);
    }
})->with(FakerProvider::$optionalDataset);
