<?php

namespace App\Domain\Kafka\Actions\Tests\Factories;

use App\Domain\Offers\Models\NameplateProduct;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductPropertyValue;

class OfferLinkedFactory
{
    public static function create(int $productId): Offer
    {
        return Offer::factory()->create([
            'product_id' => $productId,
            'updated_at' => now()->subDay(),
        ]);
    }

    public static function createFromBrand(int $brandId): Offer
    {
        return static::create(static::createProduct(['brand_id' => $brandId])->product_id);
    }

    public static function createFromCategory(int $categoryId): Offer
    {
        return static::create(static::createProduct(['category_id' => $categoryId])->product_id);
    }

    public static function createFromDirectoryValue(int $directoryValueId): Offer
    {
        $product = static::createProduct();
        ProductPropertyValue::factory()->create([
            'directory_value_id' => $directoryValueId,
            'product_id' => $product->product_id,
        ]);

        return static::create($product->product_id);
    }

    public static function createFromNameplate(int $nameplateId): Offer
    {
        $product = static::createProduct();
        NameplateProduct::factory()->create([
            'nameplate_id' => $nameplateId,
            'product_id' => $product->product_id,
        ]);

        return static::create($product->product_id);
    }

    public static function createFromProperty(int $propertyId): Offer
    {
        $product = static::createProduct();
        ProductPropertyValue::factory()->create([
            'property_id' => $propertyId,
            'product_id' => $product->product_id,
        ]);

        return static::create($product->product_id);
    }

    public static function createFromCategoryAndProperty(int $categoryId, int $propertyId): Offer
    {
        $product = static::createProduct(['category_id' => $categoryId]);
        ProductPropertyValue::factory()->create([
            'property_id' => $propertyId,
            'product_id' => $product->product_id,
        ]);

        return static::create($product->product_id);
    }

    protected static function createProduct(array $extra = []): Product
    {
        return Product::factory()->create($extra);
    }
}
