<?php

use App\Domain\Kafka\Actions\Listen\ListenProductGroupAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Category\CategoryEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroup\ProductGroupEventMessage;
use App\Domain\Offers\Models\ProductGroup;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-product-group');

test("Action ListenProductGroupAction create success", function () {
    /** @var IntegrationTestCase $this */
    $productGroupId = 1;
    $mainProductId = 1;
    $message = ProductGroupEventMessage::factory()
        ->attributes(['id' => $productGroupId, 'main_product_id' => $mainProductId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём товар и оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($mainProductId);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($mainProductId + 1);

    resolve(ListenProductGroupAction::class)->execute($message);

    assertDatabaseHas((new ProductGroup())->getTable(), [
        'product_group_id' => $productGroupId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
});

test("Action ListenProductGroupAction update success", function (array $dirty, bool $isDirty) {
    /** @var IntegrationTestCase $this */
    /** @var ProductGroup $productGroup */
    $productGroup = ProductGroup::factory()->create();
    $updatedAt = $productGroup->updated_at->addDay();
    $message = ProductGroupEventMessage::factory()
        ->attributes([
            'id' => $productGroup->product_group_id,
            'main_product_id' => $productGroup->main_product_id,
            'updated_at' => $updatedAt->toJSON(),
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём товар и оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productGroup->main_product_id);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productGroup->main_product_id + 1);

    resolve(ListenProductGroupAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $productGroup->updated_at;
    $productGroup->refresh();
    assertEquals($needUpdatedAt, $productGroup->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with([
    [['name'], true],
    [['is_active'], true],
    [['product_id'], false],
]);

test("Action ListenProductGroupAction delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var ProductGroup $productGroup */
    $productGroup = ProductGroup::factory()->create();
    $message = CategoryEventMessage::factory()
        ->attributes([
            'id' => $productGroup->product_group_id,
            'property_id' => $productGroup->main_product_id,
        ])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар и оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productGroup->main_product_id);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productGroup->main_product_id + 1);

    resolve(ListenProductGroupAction::class)->execute($message);

    assertDatabaseMissing((new ProductGroup())->getTable(), ['product_group_id' => $productGroup->product_group_id]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with(FakerProvider::$optionalDataset);
