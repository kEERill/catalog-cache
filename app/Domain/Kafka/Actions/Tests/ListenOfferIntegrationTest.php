<?php

use App\Domain\Discounts\Models\Discount;
use App\Domain\Kafka\Actions\Listen\ListenOfferAction;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Offer\OfferEventMessage;
use App\Domain\Offers\Models\Offer;
use Ensi\LaravelElasticQuery\ElasticClient;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertGreaterThan;

use function PHPUnit\Framework\assertNull;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-offer');

test("Action ListenOfferAction create success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $offerId = 1;
    $message = OfferEventMessage::factory()->attributes(['id' => $offerId])->event(ModelEventMessage::CREATE)->make();

    resolve(ListenOfferAction::class)->execute($message);

    assertDatabaseHas((new Offer())->getTable(), [
        'offer_id' => $offerId,
    ]);
})->with(FakerProvider::$optionalDataset);

test("Action ListenOfferAction update success", function (array $dirty, bool $isDirty, bool $existDiscount, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Offer $offer */
    $offer = Offer::factory()->create(['updated_at' => now()->subDay(), 'price' => 100_00]);
    if ($existDiscount) {
        Discount::factory()->for($offer)->create();
    }
    $message = OfferEventMessage::factory()
        ->attributes([
            'id' => $offer->offer_id,
            'base_price' => $offer->base_price + 1,
            'external_id' => 'external_id',
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    resolve(ListenOfferAction::class)->execute($message);

    $startUpdatedAt = $offer->updated_at;
    $offer->refresh();
    if ($isDirty) {
        assertGreaterThan($startUpdatedAt, $offer->updated_at);
        if (in_array('base_price', $dirty)) {
            $existDiscount ? assertNull($offer->price) : assertEquals($offer->price, $offer->base_price);
        }
    } else {
        assertEquals($offer->updated_at, $startUpdatedAt);
    }
})->with([
    [['base_price'], true, true],
    [['base_price'], true, false],
    [['external_id'], false, false],
], FakerProvider::$optionalDataset);

test("Action ListenOfferAction delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Offer $offer */
    $offer = Offer::factory()->create();
    $message = OfferEventMessage::factory()
        ->attributes(['id' => $offer->offer_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    $this->mock(ElasticClient::class)->shouldReceive('documentDelete')->times(1)->andReturn([]);

    resolve(ListenOfferAction::class)->execute($message);

    assertDatabaseMissing((new Offer())->getTable(), ['offer_id' => $offer->offer_id]);
})->with(FakerProvider::$optionalDataset);
