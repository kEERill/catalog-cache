<?php

use App\Domain\Kafka\Actions\Listen\ListenPublishedProductAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedProduct\PublishedProductEventMessage;
use App\Domain\Offers\Models\Product;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-published-product');

test("Action ListenPublishedProductAction create success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $productId = 1;
    $message = PublishedProductEventMessage::factory()->attributes(['id' => $productId])->event(ModelEventMessage::CREATE)->make();

    // Создаём оффер который связан с этим товаром и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productId);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productId + 1);

    resolve(ListenPublishedProductAction::class)->execute($message);

    assertDatabaseHas((new Product())->getTable(), [
        'product_id' => $productId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with(FakerProvider::$optionalDataset);

test("Action ListenPublishedProductAction update success", function (array $dirty, bool $isDirty, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Product $product */
    $product = Product::factory()->create();
    $updatedAt = $product->updated_at->addDay();
    $message = PublishedProductEventMessage::factory()
        ->attributes(['id' => $product->product_id, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём оффер который связан с этим товаром и должен пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($product->product_id);
    // Создаём прочий оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($product->product_id + 1);

    resolve(ListenPublishedProductAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $product->updated_at;
    $product->refresh();
    assertEquals($needUpdatedAt, $product->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with([
    [['name'], true],
    [['is_active'], false],
], FakerProvider::$optionalDataset);

test("Action ListenPublishedProductAction delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Product $product */
    $product = Product::factory()->create();
    $message = PublishedProductEventMessage::factory()
        ->attributes(['id' => $product->product_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём оффер, который связан с этим товаром и должен пометиться на индексацию
    $offer = OfferLinkedFactory::create($product->product_id);

    resolve(ListenPublishedProductAction::class)->execute($message);

    assertDatabaseMissing((new Product())->getTable(), ['product_id' => $product->product_id]);
    assertNewModelFieldGreaterThan($offer, 'updated_at');
})->with(FakerProvider::$optionalDataset);
