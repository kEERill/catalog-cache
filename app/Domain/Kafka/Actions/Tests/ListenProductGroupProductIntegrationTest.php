<?php

use App\Domain\Kafka\Actions\Listen\ListenProductGroupProductAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Category\CategoryEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroupProduct\ProductGroupProductEventMessage;
use App\Domain\Offers\Models\ProductGroupProduct;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-product-group-product');

test("Action ListenProductGroupProductAction create success", function () {
    /** @var IntegrationTestCase $this */
    $productGroupProductId = 1;
    $productId = 1;
    $message = ProductGroupProductEventMessage::factory()
        ->attributes(['id' => $productGroupProductId, 'product_id' => $productId])
        ->event(ModelEventMessage::CREATE)
        ->make();

    // Создаём товар и оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productId);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productId + 1);

    resolve(ListenProductGroupProductAction::class)->execute($message);

    assertDatabaseHas((new ProductGroupProduct())->getTable(), [
        'product_group_product_id' => $productGroupProductId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
});

test("Action ListenProductGroupProductAction update success", function (array $dirty, bool $isDirty) {
    /** @var IntegrationTestCase $this */
    /** @var ProductGroupProduct $productGroupProduct */
    $productGroupProduct = ProductGroupProduct::factory()->create();
    $updatedAt = $productGroupProduct->updated_at->addDay();
    $message = ProductGroupProductEventMessage::factory()
        ->attributes([
            'id' => $productGroupProduct->product_group_product_id,
            'product_id' => $productGroupProduct->product_id,
            'updated_at' => $updatedAt->toJSON(),
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём товар и оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productGroupProduct->product_id);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productGroupProduct->product_id + 1);

    resolve(ListenProductGroupProductAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $productGroupProduct->updated_at;
    $productGroupProduct->refresh();
    assertEquals($needUpdatedAt, $productGroupProduct->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with([
    [['product_id'], true],
    [['is_active'], false],
]);

test("Action ListenProductGroupProductAction delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var ProductGroupProduct $productGroupProduct */
    $productGroupProduct = ProductGroupProduct::factory()->create();
    $message = CategoryEventMessage::factory()
        ->attributes([
            'id' => $productGroupProduct->product_group_product_id,
            'property_id' => $productGroupProduct->product_id,
        ])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар и оффер, которые входят в склейку и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::create($productGroupProduct->product_id);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::create($productGroupProduct->product_id + 1);

    resolve(ListenProductGroupProductAction::class)->execute($message);

    assertDatabaseMissing((new ProductGroupProduct())->getTable(), ['product_group_product_id' => $productGroupProduct->product_group_product_id]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with(FakerProvider::$optionalDataset);
