<?php

use App\Domain\Kafka\Actions\Listen\ListenNameplateAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Nameplate\NameplateEventMessage;
use App\Domain\Offers\Models\Nameplate;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka');

test("Action ListenNameplateAction create success", function () {
    /** @var IntegrationTestCase $this */
    $nameplateId = 1;
    $message = NameplateEventMessage::factory()
        ->attributes([
            'id' => $nameplateId,
        ])
        ->event(ModelEventMessage::CREATE)
        ->make();

    resolve(ListenNameplateAction::class)->execute($message);

    assertDatabaseHas((new Nameplate())->getTable(), ['nameplate_id' => $nameplateId]);
});

test("Action ListenNameplateAction update success", function (array $dirty, bool $isDirty) {
    /** @var IntegrationTestCase $this */
    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->create();
    $updatedAt = $nameplate->updated_at->addDay();
    $message = NameplateEventMessage::factory()
        ->attributes([
            'id' => $nameplate->nameplate_id,
            'is_active' => true,
            'updated_at' => $updatedAt->toJSON(),
        ])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём товар и оффер, которые содержит тег и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::createFromNameplate($nameplate->nameplate_id);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::createFromNameplate($nameplate->nameplate_id + 1);

    resolve(ListenNameplateAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $nameplate->updated_at;
    $nameplate->refresh();
    assertEquals($needUpdatedAt, $nameplate->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with([
    [['name'], true],
    [['code'], true],
    [['background_color'], true],
    [['text_color'], true],
    [['is_active'], true],
]);

test("Action ListenNameplateAction delete success", function () {
    /** @var IntegrationTestCase $this */
    /** @var Nameplate $nameplate */
    $nameplate = Nameplate::factory()->create();
    $message = NameplateEventMessage::factory()
        ->attributes(['id' => $nameplate->nameplate_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    resolve(ListenNameplateAction::class)->execute($message);

    assertDatabaseMissing((new Nameplate())->getTable(), ['nameplate_id' => $nameplate->nameplate_id]);
});
