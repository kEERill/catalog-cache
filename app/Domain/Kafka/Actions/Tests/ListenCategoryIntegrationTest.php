<?php

use App\Domain\Kafka\Actions\Listen\ListenCategoryAction;
use App\Domain\Kafka\Actions\Tests\Factories\OfferLinkedFactory;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Category\CategoryEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Offers\Models\Category;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function PHPUnit\Framework\assertEquals;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-category');

test("Action ListenCategoryAction create success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    $categoryId = 1;
    $message = CategoryEventMessage::factory()->attributes(['id' => $categoryId])->event(ModelEventMessage::CREATE)->make();

    // Создаём товар и оффер, которые связаны с этой категорией и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::createFromCategory($categoryId);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::createFromCategory($categoryId + 1);

    resolve(ListenCategoryAction::class)->execute($message);

    assertDatabaseHas((new Category())->getTable(), [
        'category_id' => $categoryId,
    ]);
    assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with(FakerProvider::$optionalDataset);

test("Action ListenCategoryAction update success", function (array $dirty, bool $isDirty, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Category $category */
    $category = Category::factory()->create();
    $updatedAt = $category->updated_at->addDay();
    $message = CategoryEventMessage::factory()
        ->attributes(['id' => $category->category_id, 'updated_at' => $updatedAt->toJSON()])
        ->event(ModelEventMessage::UPDATE)
        ->make(['dirty' => $dirty]);

    // Создаём товар и оффер, которые связаны с этой категорией и должны пометиться на индексацию
    $offerMark = OfferLinkedFactory::createFromCategory($category->category_id);
    // Создаём прочие товар и оффер, чтобы проверить что лишнее не помечается
    $offerOther = OfferLinkedFactory::createFromCategory($category->category_id + 1);

    resolve(ListenCategoryAction::class)->execute($message);

    $needUpdatedAt = $isDirty ? $updatedAt : $category->updated_at;
    $category->refresh();
    assertEquals($needUpdatedAt, $category->updated_at);

    // Проверка аналогична созданию, но если сохранения не происходит, то и пометки быть не должно
    if ($isDirty) {
        assertNewModelFieldGreaterThan($offerMark, 'updated_at');
    } else {
        assertNewModelFieldEquals($offerMark, 'updated_at');
    }
    assertNewModelFieldEquals($offerOther, 'updated_at');
})->with([
    [['name'], true],
    [['is_active'], false],
], FakerProvider::$optionalDataset);

test("Action ListenCategoryAction delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */
    /** @var Category $category */
    $category = Category::factory()->create();
    $message = CategoryEventMessage::factory()
        ->attributes(['id' => $category->category_id])
        ->event(ModelEventMessage::DELETE)
        ->make();

    // Создаём товар и оффер, которые связаны с этой категорией и не должны пометиться на индексацию
    // т.к. при удалении не помечаем
    $offer = OfferLinkedFactory::createFromCategory($category->category_id);

    resolve(ListenCategoryAction::class)->execute($message);

    assertDatabaseMissing((new Category())->getTable(), ['category_id' => $category->category_id]);
    // Удаление произошло, но пометки нет
    assertNewModelFieldEquals($offer, 'updated_at');
})->with(FakerProvider::$optionalDataset);
