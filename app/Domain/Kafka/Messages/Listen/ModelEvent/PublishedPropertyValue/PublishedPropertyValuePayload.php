<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedPropertyValue;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $property_id ID свойства
 * @property int $product_id ID товара
 * @property string|null $type Тип значения
 * @property string|null $value Значение
 * @property string|null $name Название значения
 * @property int|null $directory_value_id ID значения справочника
 * @property CarbonInterface|null $deleted_at Время пометки на удаление
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class PublishedPropertyValuePayload extends Payload
{
    protected array $dates = ['deleted_at'];
}
