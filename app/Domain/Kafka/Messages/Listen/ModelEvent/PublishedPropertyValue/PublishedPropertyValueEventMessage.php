<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedPropertyValue;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\PublishedPropertyValueEventMessageFactory;

class PublishedPropertyValueEventMessage extends ModelEventMessage
{
    public PublishedPropertyValuePayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new PublishedPropertyValuePayload($attributes);
    }

    public static function factory(): PublishedPropertyValueEventMessageFactory
    {
        return PublishedPropertyValueEventMessageFactory::new();
    }
}
