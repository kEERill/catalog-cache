<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedProduct;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\PublishedProductEventMessageFactory;

class PublishedProductEventMessage extends ModelEventMessage
{
    public PublishedProductPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new PublishedProductPayload($attributes);
    }

    public static function factory(): PublishedProductEventMessageFactory
    {
        return PublishedProductEventMessageFactory::new();
    }
}
