<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedProduct;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property string $name Название
 * @property string $code ЧПУ код
 * @property string|null $description Описание
 * @property int $category_id Id категории
 * @property int|null $brand_id Id бренда
 * @property string|null $external_id Id товара во внешней системе
 * @property int $type Тип товара (весовой, штучный, ...) из перечисления ProductTypeEnum
 * @property int $status_id Статус товара из перечисления ProductStatus
 * @property string|null $status_comment Комментарий к статусу
 * @property bool $allow_publish Признак активности для витрины
 * @property string|null $barcode Штрихкод (EAN)
 * @property string $vendor_code Артикул
 * @property bool $is_adult Товар 18+
 * @property float|null $weight Вес нетто (кг)
 * @property float|null $weight_gross Вес брутто (кг)
 * @property float|null $width Ширина (мм)
 * @property float|null $height Высота (мм)
 * @property float|null $length Длина (мм)
 * @property string|null $main_image_file Файл основной картинки
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class PublishedProductPayload extends Payload
{
}
