<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\ProductStatusEnum;
use Ensi\PimClient\Dto\ProductTypeEnum;

class PublishedProductEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),

            'name' => $this->faker->sentence(),
            'code' => $this->faker->unique()->slug,
            'description' => $this->faker->nullable()->text,
            'category_id' => $this->faker->modelId(),
            'brand_id' => $this->faker->nullable()->modelId(),
            'external_id' => $this->faker->unique()->nullable()->numerify('######'),
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'status_id' => $this->faker->randomElement(ProductStatusEnum::getAllowableEnumValues()),
            'status_comment' => $this->faker->nullable()->text(),
            'allow_publish' => $this->faker->boolean,
            'main_image_file' => $this->faker->filePath(),

            'barcode' => $this->faker->nullable()->ean13(),
            'vendor_code' => $this->faker->numerify('###-###-###'),

            'weight' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'length' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'height' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'width' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,

            'created_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),

        ];
    }
}
