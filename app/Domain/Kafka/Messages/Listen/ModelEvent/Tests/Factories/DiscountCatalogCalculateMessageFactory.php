<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use Ensi\TestFactories\Factory;
use RdKafka\Message;

class DiscountCatalogCalculateMessageFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'product_ids' => $this->faker->randomList(fn () => $this->faker->modelId()),
        ];
    }

    public function make(array $extra = []): Message
    {
        $message = new Message();
        $message->payload = json_encode($this->makeArray($extra));

        return $message;
    }
}
