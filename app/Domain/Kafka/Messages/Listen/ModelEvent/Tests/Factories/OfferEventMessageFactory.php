<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\OffersClient\Dto\OfferSaleStatusEnum;

class OfferEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),
            'product_id' => $this->faker->modelId(),
            'discount_id' => $this->faker->modelId(),
            'seller_id' => $this->faker->modelId(),
            'external_id' => $this->faker->nullable()->bothify('###??###'),
            'sale_status' => $this->faker->randomElement(OfferSaleStatusEnum::getAllowableEnumValues()),
            'storage_address' => $this->faker->nullable()->address,
            'base_price' => $this->faker->nullable()->numberBetween(1, 10000_00),
            'created_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
        ];
    }
}
