<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class PublishedPropertyValueEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),

            'property_id' => $this->faker->unique()->modelId(),
            'product_id' => $this->faker->unique()->modelId(),

            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'name' => $this->faker->nullable()->sentence,
            'code' => $this->faker->nullable()->modelId(),
            'directory_value_id' => $this->faker->nullable()->modelId(),
            'value' => $this->faker->nullable()->word, // Тут можно написать более умный генератор, но пока незачем

            'deleted_at' => $this->faker->nullable()->date(BaseJsonResource::DATE_TIME_FORMAT),

            'created_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),

        ];
    }
}
