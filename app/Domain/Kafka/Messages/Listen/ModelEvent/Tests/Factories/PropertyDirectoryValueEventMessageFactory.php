<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class PropertyDirectoryValueEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),

            'property_id' => $this->faker->modelId(),
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'name' => $this->faker->nullable()->sentence,
            'value' => $this->faker->word, // Тут можно написать более умный генератор, но пока незачем
            'code' => $this->faker->nullable()->modelId(),

            'created_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
        ];
    }
}
