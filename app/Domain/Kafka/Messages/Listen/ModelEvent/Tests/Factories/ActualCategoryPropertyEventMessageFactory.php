<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class ActualCategoryPropertyEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),

            'property_id' => $this->faker->modelId(),
            'category_id' => $this->faker->modelId(),
            'is_required' => $this->faker->boolean,
            'is_inherited' => $this->faker->boolean,
            'is_common' => $this->faker->boolean,
            'is_gluing' => $this->faker->boolean,

            'created_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
        ];
    }
}
