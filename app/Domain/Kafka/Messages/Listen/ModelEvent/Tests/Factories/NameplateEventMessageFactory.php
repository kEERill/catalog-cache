<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class NameplateEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),
            'name' => $this->faker->text(50),
            'code' => $this->faker->unique()->text(50),
            'background_color' => $this->faker->hexColor(),
            'text_color' => $this->faker->hexColor(),
            'is_active' => $this->faker->boolean(),
            'created_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
        ];
    }
}
