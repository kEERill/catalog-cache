<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class CategoryEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),
            'name' => $this->faker->word,
            'code' => $this->faker->unique()->regexify('[A-Za-z0-9_]{30}'),
            'parent_id' => $this->faker->nullable()->modelId(),
            'is_active' => $this->faker->boolean,
            'is_inherits_properties' => $this->faker->boolean,
            'is_real_active' => $this->faker->boolean,
            'full_code' => null,
            'actualized_at' => $this->faker->nullable()->date(BaseJsonResource::DATE_TIME_FORMAT),
            'created_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
        ];
    }
}
