<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class PropertyEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),

            'name' => $this->faker->word,
            'display_name' => $this->faker->word,
            'code' => $this->faker->unique()->slug(2),
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'is_multiple' => $this->faker->boolean,
            'is_filterable' => $this->faker->boolean,
            'is_active' => $this->faker->boolean,
            'hint_value' => $this->faker->sentence,
            'hint_value_name' => $this->faker->sentence,
            'is_common' => $this->faker->boolean,
            'is_required' => $this->faker->boolean,
            'is_system' => $this->faker->boolean,
            'has_directory' => $this->faker->boolean,
            'is_moderated' => $this->faker->boolean,
            'is_public' => $this->faker->boolean,

            'created_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
        ];
    }
}
