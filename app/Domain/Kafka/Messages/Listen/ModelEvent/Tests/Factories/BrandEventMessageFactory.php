<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories;

use App\Http\ApiV1\Support\Resources\BaseJsonResource;

class BrandEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        $logoFile = $this->faker->nullable()->filePath();

        return [
            'id' => $this->faker->unique()->modelId(),
            'name' => $this->faker->company,
            'code' => $this->faker->slug,
            'description' => $this->faker->nullable()->sentence,
            'is_active' => $this->faker->nullable()->boolean,
            'logo_file' => $logoFile,
            'logo_url' => $this->faker->nullable($logoFile ? 1 : 0)->imageUrl,
            'created_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
            'updated_at' => $this->faker->date(BaseJsonResource::DATE_TIME_FORMAT),
        ];
    }
}
