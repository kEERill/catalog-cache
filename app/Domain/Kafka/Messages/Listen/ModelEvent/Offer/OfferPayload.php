<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Offer;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $product_id - id товара
 * @property int $seller_id - id продавца
 * @property string|null $external_id - id товара у продавца
 * @property int $sale_status - статус продажи
 * @property string|null $storage_address - адрес хранения товара в магазине (для сборщика)
 * @property int|null $base_price - базовая цена
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class OfferPayload extends Payload
{
}
