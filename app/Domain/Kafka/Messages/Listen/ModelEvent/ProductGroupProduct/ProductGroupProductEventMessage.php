<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroupProduct;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\ProductGroupProductEventMessageFactory;

class ProductGroupProductEventMessage extends ModelEventMessage
{
    public ProductGroupProductPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new ProductGroupProductPayload($attributes);
    }

    public static function factory(): ProductGroupProductEventMessageFactory
    {
        return ProductGroupProductEventMessageFactory::new();
    }
}
