<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroupProduct;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $product_group_id Id товарной склейки
 * @property int $product_id Id товара
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class ProductGroupProductPayload extends Payload
{
}
