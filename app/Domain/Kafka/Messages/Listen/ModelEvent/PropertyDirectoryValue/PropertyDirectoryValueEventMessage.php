<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\PropertyDirectoryValue;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\PropertyDirectoryValueEventMessageFactory;

class PropertyDirectoryValueEventMessage extends ModelEventMessage
{
    public PropertyDirectoryValuePayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new PropertyDirectoryValuePayload($attributes);
    }

    public static function factory(): PropertyDirectoryValueEventMessageFactory
    {
        return PropertyDirectoryValueEventMessageFactory::new();
    }
}
