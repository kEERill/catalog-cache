<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Category;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property string $name Название
 * @property string $code ЧПУ код категории
 * @property int $parent_id Id родительской категории
 * @property bool $is_active Признак активности, устанавливаемый пользователями
 * @property bool $is_inherits_properties Признак наследования атрибутов родительской категории
 * @property bool $is_real_active Признак активности с учетом иерархии
 * @property array|null $full_code Массив кодов родительских и данной категории
 * @property CarbonInterface|null $actualized_at Метка времени последней актуализации категории и ее свойств
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class CategoryPayload extends Payload
{
    protected array $dates = ['actualized_at'];
}
