<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Brand;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\BrandEventMessageFactory;

class BrandEventMessage extends ModelEventMessage
{
    public BrandPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new BrandPayload($attributes);
    }

    public static function factory(): BrandEventMessageFactory
    {
        return BrandEventMessageFactory::new();
    }
}
