<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\ProductGroup;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\ProductGroupEventMessageFactory;

class ProductGroupEventMessage extends ModelEventMessage
{
    public ProductGroupPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new ProductGroupPayload($attributes);
    }

    public static function factory(): ProductGroupEventMessageFactory
    {
        return ProductGroupEventMessageFactory::new();
    }
}
