<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\PublishedImage;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $product_id Id товара
 * @property int $sort Порядок сортировки
 * @property string|null $file Файл картинки
 * @property string|null $name Описание картинки
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class PublishedImagePayload extends Payload
{
}
