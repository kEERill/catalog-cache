<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\ActualCategoryProperty;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\ActualCategoryPropertyEventMessageFactory;

class ActualCategoryPropertyEventMessage extends ModelEventMessage
{
    public ActualCategoryPropertyPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new ActualCategoryPropertyPayload($attributes);
    }

    public static function factory(): ActualCategoryPropertyEventMessageFactory
    {
        return ActualCategoryPropertyEventMessageFactory::new();
    }
}
