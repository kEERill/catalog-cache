<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\ActualCategoryProperty;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $property_id Id свойства
 * @property int $category_id Id категории
 * @property bool $is_gluing Свойство используется для склейки товаров
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class ActualCategoryPropertyPayload extends Payload
{
}
