<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\NameplateProduct;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use Carbon\CarbonInterface;

/**
 * @property int $id
 *
 * @property int $nameplate_id - идентификатор тега
 * @property int $product_id - идентификатор товара
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 */
class NameplateProductPayload extends Payload
{
}
