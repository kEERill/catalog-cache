<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\NameplateProduct;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\NameplateProductEventMessageFactory;

class NameplateProductEventMessage extends ModelEventMessage
{
    public NameplateProductPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new NameplateProductPayload($attributes);
    }

    public static function factory(): NameplateProductEventMessageFactory
    {
        return NameplateProductEventMessageFactory::new();
    }
}
