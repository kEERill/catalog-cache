<?php

namespace App\Domain\Elastic\Concerns;

use App\Exceptions\ExceptionFormatter;
use Ensi\LaravelElasticQuery\ElasticIndex as BaseElasticIndex;
use Throwable;

abstract class ElasticIndex extends BaseElasticIndex
{
    public static function parseHashFromIndexName(string $indexName): string
    {
        $ar = explode('_', $indexName);

        return end($ar);
    }

    public function indexName(): string
    {
        return $this->staticIndexName() . '_' . $this->settingsHash();
    }

    public function staticIndexName(): string
    {
        return strtolower(str_replace(['.', '-', ' '], '', join('_', [
            static::indexNamePrefix(),
            $this->indexBaseName(),
        ])));
    }

    public function indexBaseName(): string
    {
        return $this->name;
    }

    public static function indexNamePrefix(): string
    {
        return strtolower(str_replace(['.', '-', ' '], '', join('_', [
            config('app.name'),
            config('app.stage'),
        ])));
    }

    public function settingsHash(): string
    {
        return substr(md5(json_encode($this->settings())), 0, 8);
    }

    public function createIfNotExist(): ?bool
    {
        $logger = logger()->channel('elastic:create');

        try {
            if ($this->isCreated()) {
                return null;
            }

            $logger->info("Индекс: {$this->indexName()}. Начинаю создание");
            $this->create();
            $logger->info("Индекс: {$this->indexName()}. Создан");

            return true;
        } catch (Throwable $e) {
            $logger->error("Индекс: {$this->indexName()}. " . ExceptionFormatter::line("Ошибка при создании", $e));

            return false;
        }
    }
}
