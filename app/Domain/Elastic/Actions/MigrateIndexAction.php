<?php

namespace App\Domain\Elastic\Actions;

use App\Domain\Elastic\Concerns\ElasticIndex;

class MigrateIndexAction
{
    public function __construct(
    ) {
    }

    public function execute(ElasticIndex $index): void
    {
        if ($index->isCreated()) {
            return;
        }

        $index->create();
    }
}
