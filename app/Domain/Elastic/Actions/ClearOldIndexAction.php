<?php

namespace App\Domain\Elastic\Actions;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Elastic\Models\IndexerTimestamp;
use App\Exceptions\ExceptionFormatter;
use Psr\Log\LoggerInterface;
use Throwable;

class ClearOldIndexAction
{
    protected LoggerInterface $logger;

    public function __construct()
    {
        $this->logger = logger()->channel('elastic:clear');
    }

    public function execute(ElasticIndex $index): void
    {
        $indices = $index->catIndices($index->staticIndexName(), ['creation.date', 'index']);

        $indices = collect($indices)->sortBy(function ($index) {
            return $index['creation.date'];
        });

        if ($indices->count() <= 2) {
            return;
        }

        $indices->pop(2);

        foreach ($indices as $indexInfo) {
            $indexName = $indexInfo['index'];
            $this->logger->info("Индекс: {$indexName}. Начинаю удаление");

            try {
                $index->indicesDelete($indexName);
                $this->logger->info("Индекс: {$indexName}. Индекс удалён");

                $stage = config('app.stage');
                $hash = ElasticIndex::parseHashFromIndexName($indexName);
                $this->logger->info("Индекс: {$indexName}. Удаляю IndexerTimestamp, index: {$index->indexBaseName()}, stage: {$stage}, hash: {$hash}");
                $count = IndexerTimestamp::query()->whereIndex($index, $hash)->delete();
                $this->logger->info("Индекс: {$indexName}. Удалено записей: {$count}");
            } catch (Throwable $e) {
                $this->logger->error("Индекс: {$indexName}. " . ExceptionFormatter::line("Ошибка при удалении", $e));
            }
        }
    }
}
