<?php

namespace App\Domain\Elastic\Actions;

use App\Domain\Elastic\Concerns\ElasticIndex;

class ApplyIndexAction
{
    public function execute(callable $fn): void
    {
        foreach (config('elastic.indexes') as $indexClass => $settings) {
            /** @var ElasticIndex $index */
            $index = new $indexClass();

            $fn($index);
        }
    }
}
