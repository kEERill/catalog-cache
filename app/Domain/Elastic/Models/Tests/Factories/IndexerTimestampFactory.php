<?php

namespace App\Domain\Elastic\Models\Tests\Factories;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Elastic\Models\IndexerTimestamp;
use Tests\Factories\BaseModelFactory;

class IndexerTimestampFactory extends BaseModelFactory
{
    protected $model = IndexerTimestamp::class;

    public function definition(): array
    {
        return [
            'index' => $this->faker->word(),
            'stage' => config('app.stage'),
            'index_hash' => $this->faker->ean8(),
            'last_schedule' => $this->faker->dateTime(),
        ];
    }

    public function forIndex(ElasticIndex $index): static
    {
        return $this->state([
            'index' => $index->indexBaseName(),
            'index_hash' => $index->settingsHash(),
        ]);
    }
}
