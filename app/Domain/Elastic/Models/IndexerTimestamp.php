<?php

namespace App\Domain\Elastic\Models;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Elastic\Models\Tests\Factories\IndexerTimestampFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property string $index базовае название интекса (products, categories)
 * @property string $stage ветка, на которой происходит индексация
 * @property string $index_hash хеш индекса
 *
 * @property CarbonInterface $last_schedule
 */
class IndexerTimestamp extends Model
{
    protected $table = 'indexer_timestamps';

    public $timestamps = false;

    protected $dates = ['last_schedule'];

    public static function factory(): IndexerTimestampFactory
    {
        return IndexerTimestampFactory::new();
    }

    public function scopeWhereIndex(Builder $builder, ElasticIndex $index, ?string $hash = null): Builder
    {
        return $builder->where('index', $index->indexBaseName())
            ->where('stage', config('app.stage'))
            ->where('index_hash', $hash ?: $index->settingsHash());
    }

    public static function makeForIndex(ElasticIndex $index): static
    {
        $model = new static();
        $model->index = $index->indexBaseName();
        $model->stage = config('app.stage');
        $model->index_hash = $index->settingsHash();

        return $model;
    }
}
