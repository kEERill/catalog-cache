<?php

namespace App\Domain\Offers\Jobs;

use App\Domain\Offers\Actions\IndexingOffersAction;
use App\Domain\Offers\Models\Offer;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;

class IndexingOffersJob implements ShouldQueue
{
    use Dispatchable;
    use InteractsWithQueue;
    use Queueable;
    use SerializesModels;

    public function __construct(public readonly array $ids)
    {
        $stage = config('app.stage');
        $this->queue = "{{$stage}-indexing-offers}";
    }

    public function handle(IndexingOffersAction $action): void
    {
        /** @var Collection<Offer> $offers */
        $offers = Offer::queryForIndexing()
            ->whereIn('offer_id', $this->ids)
            ->get();

        $action->execute($offers);
    }
}
