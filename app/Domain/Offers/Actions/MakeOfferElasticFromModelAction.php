<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Offers\Elastic\Data\BrandData;
use App\Domain\Offers\Elastic\Data\CategoryData;
use App\Domain\Offers\Elastic\Data\DiscountData;
use App\Domain\Offers\Elastic\Data\GluingData;
use App\Domain\Offers\Elastic\Data\GluingPropertyData;
use App\Domain\Offers\Elastic\Data\ImageData;
use App\Domain\Offers\Elastic\Data\NameplateData;
use App\Domain\Offers\Elastic\Data\OfferData;
use App\Domain\Offers\Elastic\Data\PropertyData;
use App\Domain\Offers\Elastic\Data\PropertyValueData;
use App\Domain\Offers\Models\Image;
use App\Domain\Offers\Models\Offer;
use Ensi\LaravelEnsiFilesystem\Models\EnsiFile;

class MakeOfferElasticFromModelAction
{
    public function execute(Offer $offer): ?OfferData
    {
        if (!$offer->product) {
            return null;
        }
        $data = new OfferData();
        $data->offer_id = $offer->offer_id;
        $data->product_id = $offer->product_id;
        $data->cost = $offer->base_price;
        $data->price = $offer->price ?? $data->cost;

        $data->allow_publish = $offer->product->allow_publish;
        $data->main_image_file = $offer->product->main_image_file;
        $data->category_id = $offer->product->category_id;
        $data->brand_id = $offer->product->brand_id;
        $data->name = $offer->product->name;
        $data->name_sort = $offer->product->name;
        $data->code = $offer->product->code;
        $data->description = $offer->product->description;
        $data->type = $offer->product->type;
        $data->vendor_code = $offer->product->vendor_code;
        $data->barcode = $offer->product->barcode;
        $data->weight = $offer->product->weight;
        $data->weight_gross = $offer->product->weight_gross;
        $data->length = $offer->product->length;
        $data->width = $offer->product->width;
        $data->height = $offer->product->height;
        $data->is_adult = $offer->product->is_adult;
        $data->gluing_name = $offer->product->productGroup?->name;
        $data->gluing_is_main = !$offer->product->productGroup || $offer->product->productGroup->main_product_id == $offer->product_id;
        $data->gluing_is_active = $offer->product->productGroup?->is_active ?? $offer->product->allow_publish;

        $data->discount = $this->makeDiscount($offer);

        $data->brand = $this->makeBrand($offer);
        if ($offer->product->brand_id && !$data->brand) {
            return null;
        }

        $data->category = $this->makeCategory($offer);
        if (!$data->category) {
            return null;
        }

        $images = [];
        foreach ($offer->product->images as $image) {
            $imageData = $this->makeImage($image);
            if ($imageData) {
                $images[] = $imageData;
            }
        }
        $data->images = $images;

        $data->props = $this->makeProperties($offer);

        $data->nameplates = $this->makeNameplates($offer);

        $data->gluing = $this->makeGluing($offer);

        return $data;
    }

    protected function makeDiscount(Offer $offer): ?DiscountData
    {
        $discount = $offer->price ? $offer->discount : null;
        if (!$discount) {
            return null;
        }

        $discountData = new DiscountData();
        $discountData->value_type = $discount->value_type;
        $discountData->value = $discount->value;

        return $discountData;
    }

    protected function makeBrand(Offer $offer): ?BrandData
    {
        $brand = $offer->product->brand;
        if (!$brand) {
            return null;
        }

        $brandData = new BrandData();
        $brandData->id = $brand->brand_id;
        $brandData->name = $brand->name;
        $brandData->code = $brand->code;
        $brandData->description = $brand->description;
        $brandData->logo_file = $brand->logo_file;
        $brandData->logo_url = $brand->logo_url;

        return $brandData;
    }

    protected function makeCategory(Offer $offer): ?CategoryData
    {
        $category = $offer->product->category;
        if (!$category) {
            return null;
        }

        $categoryData = new CategoryData();
        $categoryData->id = $category->category_id;
        $categoryData->name = $category->name;
        $categoryData->code = $category->code;
        $categoryData->parent_id = $category->parent_id;

        return $categoryData;
    }

    protected function makeImage(Image $image): ?ImageData
    {
        $imageData = new ImageData();
        $imageData->id = $image->id;
        $imageData->name = $image->name;
        $imageData->sort = $image->sort;
        $imageData->url = EnsiFile::public($image->file)->getUrl();

        if (!$imageData->url) {
            return null;
        }

        return $imageData;
    }

    protected function makeProperties(Offer $offer): array
    {
        $properties = [];
        foreach ($offer->product->productPropertyValues as $propertyValue) {
            $property = $propertyValue->property;
            if (!$property || !$property->is_public || !$property->is_active) {
                continue;
            }

            $propValue = new PropertyValueData();
            $propValueAndName = $propertyValue->getPropValueAndName();
            if (!$propValueAndName) {
                continue;
            }
            $propValue->value = $propValueAndName[0];
            $propValue->name = $propValueAndName[1];

            if (!isset($properties[$property->property_id])) {
                $attribute = new PropertyData();
                $attribute->type = $property->type;
                $attribute->name = $property->name;
                $attribute->code = $property->code;
                $attribute->values = [];
                $properties[$propertyValue->property_id] = $attribute;
            }

            $properties[$propertyValue->property_id]->addValue($propValue);
        }

        return array_values($properties);
    }

    protected function makeGluing(Offer $offer): array
    {
        $gluing = [];
        $productGroup = $offer->product->productGroup;
        if (!$productGroup) {
            return $gluing;
        }

        $gluingPropertyIds = $offer
            ->product
            ->category
            ->actualProperties
            ->where('is_gluing', true)
            ->pluck('property_id')
            ->all();
        if (!$gluingPropertyIds) {
            return $gluing;
        }

        foreach ($offer->product->productGroup->products as $product) {
            if ($product->product_id == $offer->product_id) {
                continue;
            }

            $gluingOffer = $product->offers->first();
            if (!$gluingOffer) {
                continue;
            }

            $gluingData = new GluingData();
            $gluing[] = $gluingData;
            $gluingData->id = $gluingOffer->offer_id;
            $gluingData->name = $product->name;
            $gluingData->code = $product->code;

            foreach ($product->productPropertyValues as $propertyValue) {
                $property = $propertyValue->property;
                if (!$property ||
                    !$property->is_public ||
                    !$property->is_active ||
                    !in_array($property->property_id, $gluingPropertyIds)
                ) {
                    continue;
                }

                $gluingPropertyData = new GluingPropertyData();
                $gluingData->addProp($gluingPropertyData);
                $gluingPropertyData->prop_type = $property->type;
                $gluingPropertyData->prop_name = $property->name;
                $gluingPropertyData->prop_code = $property->code;
                $propValueAndName = $propertyValue->getPropValueAndName();
                if (!$propValueAndName) {
                    continue;
                }
                $gluingPropertyData->value_value = $propValueAndName[0];
                $gluingPropertyData->value_name = $propValueAndName[1];
            }
        }

        return $gluing;
    }

    private function makeNameplates(Offer $offer): array
    {
        $nameplates = [];
        foreach ($offer->product->nameplates ?? [] as $nameplate) {
            if (!$nameplate->is_active) {
                continue;
            }

            $nameplateData = new NameplateData();
            $nameplateData->id = $nameplate->nameplate_id;
            $nameplateData->name = $nameplate->name;
            $nameplateData->code = $nameplate->code;
            $nameplateData->background_color = $nameplate->background_color;
            $nameplateData->text_color = $nameplate->text_color;

            $nameplates[] = $nameplateData;
        }

        return $nameplates;
    }
}
