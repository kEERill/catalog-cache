<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Offers\Models\Offer;

class MarkOfferToIndexingAction
{
    public function execute(?callable $filter = null): void
    {
        $query = Offer::query();

        if ($filter) {
            $filter($query);
        }

        $query->update(['updated_at' => now()]);
    }
}
