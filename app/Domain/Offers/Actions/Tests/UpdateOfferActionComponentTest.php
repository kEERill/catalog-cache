<?php

use App\Domain\Discounts\Models\Discount;
use App\Domain\Offers\Actions\UpdateOfferAction;
use App\Domain\Offers\Models\Offer;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseMissing;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertGreaterThan;
use function PHPUnit\Framework\assertNull;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'offers');

test("Action UpdateOfferAction update base_price", function (bool $existDiscount, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ComponentTestCase $this */
    /** @var Offer $offer */
    $offer = Offer::factory()->withPrice()->create(['updated_at' => now()->subDay()]);
    $startUpdatedAt = $offer->updated_at;
    /** @var Discount $discount */
    if ($existDiscount) {
        $discount = Discount::factory()->for($offer)->create();
    }

    resolve(UpdateOfferAction::class)->execute(
        offer: $offer,
        fillModel: fn (Offer $offer) => $offer->base_price = $offer->base_price + 5_00,
    );

    $offer->refresh();
    if ($existDiscount) {
        assertNull($offer->price);
    } else {
        assertEquals($offer->price, $offer->base_price);
    }
    assertGreaterThan($startUpdatedAt, $offer->updated_at);

    if ($existDiscount) {
        assertDatabaseMissing((new Discount())->getTable(), ['id' => $discount->id]);
    }
})->with([
    [true],
    [false],
], FakerProvider::$optionalDataset);
