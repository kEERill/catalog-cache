<?php

use App\Domain\Offers\Jobs\IndexingOffersJob;
use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use Ensi\LaravelElasticQuery\ElasticClient;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;

use PHPUnit\Framework\Assert;


use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'offers');

test("Action ProcessOffersAction error check marketing price", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var ComponentTestCase $this */

    /** @var Category $category */
    $category = Category::factory()->create();

    /** @var Offer $offerWithCalc */
    $offerWithCalc = Offer::factory()->withBasePrice()->create(['price' => null]);

    Product::factory()
        ->create(['category_id' => $category->category_id, 'product_id' => $offerWithCalc->product_id, 'brand_id' => null]);

    $this->mockMarketingCalculatorsApi()
        ->shouldReceive('calculateCatalog')
        ->once()
        ->andThrow(new Exception("Не удалось подсчитать стоимость товара"));

    $mock = $this->mock(ElasticClient::class);
    $mock->shouldReceive('bulk')->withArgs(function (string $index, array $body) use ($offerWithCalc) {
        $offer = json_decode($body[1], true);
        Assert::assertTrue($offer['price'] == $offerWithCalc->base_price);

        return true;
    });

    IndexingOffersJob::dispatchSync([$offerWithCalc->offer_id]);

    assertDatabaseHas((new Offer())->getTable(), ['offer_id' => $offerWithCalc->offer_id, 'price' => null]);
})->with(FakerProvider::$optionalDataset);
