<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Offers\Models\Offer;
use Illuminate\Database\Eloquent\Collection as DatabaseCollection;

class UpdateOffersAction
{
    public function __construct(protected UpdateOfferAction $updateOfferAction)
    {
    }

    public function execute(DatabaseCollection $offers, callable $fillModel, ?callable $error = null, bool $save = true): void
    {
        $offers->loadMissing('discount');

        /** @var Offer $offer */
        foreach ($offers as $offer) {
            $this->updateOfferAction->execute(
                offer: $offer,
                fillModel: $fillModel,
                error: $error,
                save: $save,
            );
        }
    }
}
