<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Offers\Models\Offer;
use Throwable;

class UpdateOfferAction
{
    public function execute(Offer $offer, callable $fillModel, ?callable $error = null, bool $save = true): void
    {
        try {
            $fillModel($offer);

            $this->resetMarketingData($offer);

            if ($offer->isDirty() && $save) {
                $offer->save();
            }
        } catch (Throwable $e) {
            $error ? $error($offer, $e) : throw $e;
        }
    }

    protected function resetMarketingData(Offer $model): void
    {
        //пересчитываем цену, только при наличии скидки
        if ($model->isDirty(['base_price'])) {
            $model->loadMissing('discount');
            $model->price = $model->discount ? null : $model->base_price;
        }

        //если цену обнулили удаляем скидку
        if ($model->isDirty(['price']) && $model->price == null) {
            $model->loadMissing('discount');
            $model->discount?->delete();
        }
    }
}
