<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Offers\Elastic\OfferIndex;

class DeleteOfferFromIndexAction
{
    public function execute(int $id): void
    {
        $index = new OfferIndex();
        $index->documentDelete($id);
    }
}
