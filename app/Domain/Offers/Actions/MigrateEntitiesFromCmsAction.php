<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Offers\Models\Nameplate;
use App\Domain\Offers\Models\NameplateProduct;
use App\Domain\Support\Concerns\MigrateEntities;
use Ensi\CmsClient\Api\NameplatesApi;
use Ensi\CmsClient\Dto\PaginationTypeEnum as CmsPaginationTypeEnum;
use Ensi\CmsClient\Dto\RequestBodyPagination as CmsRequestBodyPagination;
use Ensi\CmsClient\Dto\SearchNameplatesRequest;
use Illuminate\Support\Arr;

class MigrateEntitiesFromCmsAction
{
    use MigrateEntities;

    /** @var int - количество записей получаемых за один запрос */
    public const LIMIT = 50;

    public function __construct(
        protected readonly NameplatesApi $nameplatesApi,
    ) {
    }

    public function execute(): void
    {
        $this->loadNameplates();
    }

    private function loadNameplates(): void
    {
        $this->executeBeforeMigration([Nameplate::class, NameplateProduct::class]);

        $request = new SearchNameplatesRequest();
        $request->setInclude(['product_links']);
        $request->setFilter((object)['is_active' => true]);
        $request->setPagination((new CmsRequestBodyPagination())->setLimit(self::LIMIT)->setType(CmsPaginationTypeEnum::CURSOR));
        while (true) {
            $response = $this->nameplatesApi->searchNameplates($request);

            $nameplates = Nameplate::query()
                ->whereIn('nameplate_id', Arr::pluck($response->getData(), 'id'))
                ->get()
                ->keyBy('nameplate_id');
            $nameplateProducts = NameplateProduct::query()
                ->whereIn(
                    'nameplate_product_id',
                    array_filter(Arr::collapse(Arr::pluck($response->getData(), 'product_links.*.id')))
                )
                ->get()
                ->keyBy('nameplate_product_id');

            foreach ($response->getData() as $nameplateCms) {
                $nameplate = $nameplates->get($nameplateCms->getId());
                if (!$nameplate) {
                    $nameplate = new Nameplate();
                    $nameplate->nameplate_id = $nameplateCms->getId();
                }
                $nameplate->name = $nameplateCms->getName();
                $nameplate->code = $nameplateCms->getCode();
                $nameplate->background_color = $nameplateCms->getBackgroundColor();
                $nameplate->text_color = $nameplateCms->getTextColor();
                $nameplate->created_at = $nameplateCms->getCreatedAt();
                $nameplate->updated_at = $nameplateCms->getUpdatedAt();
                $nameplate->is_migrated = true;
                $nameplate->save();

                $productLinks = $nameplateCms->getProductLinks();
                if ($productLinks) {
                    foreach ($productLinks as $productLinkPim) {
                        $nameplateProduct = $nameplateProducts->get($productLinkPim->getId());
                        if (!$nameplateProduct) {
                            $nameplateProduct = new NameplateProduct();
                            $nameplateProduct->nameplate_product_id = $productLinkPim->getId();
                        }
                        $nameplateProduct->nameplate_id = $productLinkPim->getNameplateId();
                        $nameplateProduct->product_id = $productLinkPim->getProductId();
                        $nameplateProduct->created_at = $productLinkPim->getCreatedAt();
                        $nameplateProduct->updated_at = $productLinkPim->getUpdatedAt();
                        $nameplateProduct->is_migrated = true;
                        $nameplateProduct->save();
                    }
                }
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            if (!$nextCursor) {
                break;
            }

            $request->getPagination()->setCursor($nextCursor);
        }

        $this->executeAfterMigration([Nameplate::class, NameplateProduct::class]);
    }
}
