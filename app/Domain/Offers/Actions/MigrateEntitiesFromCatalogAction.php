<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Offers\Models\ActualCategoryProperty;
use App\Domain\Offers\Models\Brand;
use App\Domain\Offers\Models\Category;
use App\Domain\Offers\Models\Image;
use App\Domain\Offers\Models\Offer;
use App\Domain\Offers\Models\Product;
use App\Domain\Offers\Models\ProductGroup;
use App\Domain\Offers\Models\ProductGroupProduct;
use App\Domain\Offers\Models\ProductPropertyValue;
use App\Domain\Offers\Models\Property;
use App\Domain\Offers\Models\PropertyDirectoryValue;
use App\Domain\Support\Concerns\MigrateEntities;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\PaginationTypeEnum as OffersPaginationTypeEnum;
use Ensi\OffersClient\Dto\RequestBodyPagination as OffersRequestBodyPagination;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\PimClient\Api\BrandsApi;
use Ensi\PimClient\Api\CategoriesApi;
use Ensi\PimClient\Api\ProductGroupsApi;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Api\PropertiesApi;
use Ensi\PimClient\Dto\PaginationTypeEnum as PimPaginationTypeEnum;
use Ensi\PimClient\Dto\RequestBodyPagination as PimRequestBodyPagination;
use Ensi\PimClient\Dto\SearchBrandsRequest;
use Ensi\PimClient\Dto\SearchCategoriesRequest;
use Ensi\PimClient\Dto\SearchDirectoryValuesRequest;
use Ensi\PimClient\Dto\SearchProductGroupsRequest;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Ensi\PimClient\Dto\SearchPropertiesRequest;
use Illuminate\Support\Arr;

class MigrateEntitiesFromCatalogAction
{
    use MigrateEntities;

    public function __construct(
        protected readonly BrandsApi $brandsApi,
        protected readonly CategoriesApi $categoriesApi,
        protected readonly OffersApi $offersApi,
        protected readonly PropertiesApi $propertiesApi,
        protected readonly ProductsApi $productsApi,
        protected readonly ProductGroupsApi $productGroupsApi,
    ) {
    }

    public function execute(): void
    {
        $this->loadBrands();
        $this->loadCategories();
        $this->loadProperties();
        $this->loadPropertyDirectoryValues();
        $this->loadProducts();
        $this->loadOffers();
        $this->loadProductGroups();
    }

    protected function loadBrands(): void
    {
        $this->executeBeforeMigration([Brand::class]);

        $request = new SearchBrandsRequest();
        $request->setPagination((new PimRequestBodyPagination())->setLimit(50)->setType(PimPaginationTypeEnum::CURSOR));
        while (true) {
            $response = $this->brandsApi->searchBrands($request);

            $brands = Brand::query()
                ->whereIn('brand_id', Arr::pluck($response->getData(), 'id'))
                ->get()
                ->keyBy('brand_id');
            foreach ($response->getData() as $brandPim) {
                $brand = $brands->get($brandPim->getId());
                if (!$brand) {
                    $brand = new Brand();
                    $brand->brand_id = $brandPim->getId();
                }
                $brand->name = $brandPim->getName();
                $brand->code = $brandPim->getCode();
                $brand->description = $brandPim->getDescription();
                $brand->logo_file = $brandPim->getLogoFile()?->getPath();
                $brand->logo_url = $brandPim->getLogoUrl();
                $brand->created_at = $brandPim->getCreatedAt();
                $brand->updated_at = $brandPim->getUpdatedAt();
                $brand->is_migrated = true;
                $brand->save();
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            if (!$nextCursor) {
                break;
            }

            $request->getPagination()->setCursor($nextCursor);
        }

        $this->executeAfterMigration([Brand::class]);
    }

    protected function loadCategories(): void
    {
        $this->executeBeforeMigration([Category::class, ActualCategoryProperty::class]);

        $request = new SearchCategoriesRequest();
        $request->setInclude(['properties']);
        $request->setPagination((new PimRequestBodyPagination())->setLimit(50)->setType(PimPaginationTypeEnum::CURSOR));
        while (true) {
            $response = $this->categoriesApi->searchCategories($request);

            $categories = Category::query()
                ->whereIn('category_id', Arr::pluck($response->getData(), 'id'))
                ->get()
                ->keyBy('category_id');
            $actualCategoryProperties = ActualCategoryProperty::query()
                ->whereIn(
                    'actual_category_property_id',
                    array_filter(Arr::collapse(Arr::pluck($response->getData(), 'properties.*.id')))
                )
                ->get()
                ->keyBy('actual_category_property_id');
            foreach ($response->getData() as $categoryPim) {
                $category = $categories->get($categoryPim->getId());
                if (!$category) {
                    $category = new Category();
                    $category->category_id = $categoryPim->getId();
                }
                $category->name = $categoryPim->getName();
                $category->code = $categoryPim->getCode();
                $category->parent_id = $categoryPim->getParentId();
                $category->created_at = $categoryPim->getCreatedAt();
                $category->updated_at = $categoryPim->getUpdatedAt();
                $category->is_migrated = true;
                $category->save();

                $properties = $categoryPim->getProperties();
                if ($properties) {
                    foreach ($properties as $propertyPim) {
                        $actualCategoryProperty = $actualCategoryProperties->get($propertyPim->getId());
                        if (!$actualCategoryProperty) {
                            $actualCategoryProperty = new ActualCategoryProperty();
                            $actualCategoryProperty->actual_category_property_id = $propertyPim->getId();
                        }
                        $actualCategoryProperty->category_id = $categoryPim->getId();
                        $actualCategoryProperty->property_id = $propertyPim->getPropertyId();
                        $actualCategoryProperty->is_gluing = $propertyPim->getIsGluing();
                        $actualCategoryProperty->created_at = now();
                        $actualCategoryProperty->updated_at = now();
                        $actualCategoryProperty->is_migrated = true;
                        $actualCategoryProperty->save();
                    }
                }
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            if (!$nextCursor) {
                break;
            }

            $request->getPagination()->setCursor($nextCursor);
        }

        $this->executeAfterMigration([Category::class, ActualCategoryProperty::class]);
    }

    protected function loadProperties(): void
    {
        $this->executeBeforeMigration([Property::class]);

        $request = new SearchPropertiesRequest();
        $request->setPagination((new PimRequestBodyPagination())->setLimit(50)->setType(PimPaginationTypeEnum::CURSOR));
        while (true) {
            $response = $this->propertiesApi->searchProperties($request);

            $properties = Property::query()
                ->whereIn('property_id', Arr::pluck($response->getData(), 'id'))
                ->get()
                ->keyBy('property_id');
            foreach ($response->getData() as $propertyPim) {
                $property = $properties->get($propertyPim->getId());
                if (!$property) {
                    $property = new Property();
                    $property->property_id = $propertyPim->getId();
                }
                $property->name = $propertyPim->getDisplayName();
                $property->code = $propertyPim->getCode();
                $property->type = $propertyPim->getType();
                $property->is_public = $propertyPim->getIsPublic();
                $property->is_active = $propertyPim->getIsActive();
                $property->created_at = $propertyPim->getCreatedAt();
                $property->updated_at = $propertyPim->getUpdatedAt();
                $property->is_migrated = true;
                $property->save();
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            if (!$nextCursor) {
                break;
            }

            $request->getPagination()->setCursor($nextCursor);
        }

        $this->executeAfterMigration([Property::class]);
    }

    protected function loadPropertyDirectoryValues(): void
    {
        $this->executeBeforeMigration([PropertyDirectoryValue::class]);

        $request = new SearchDirectoryValuesRequest();
        $request->setPagination((new PimRequestBodyPagination())->setLimit(50)->setType(PimPaginationTypeEnum::CURSOR));
        while (true) {
            $response = $this->propertiesApi->searchDirectoryValues($request);

            $directoryValues = PropertyDirectoryValue::query()
                ->whereIn('directory_value_id', Arr::pluck($response->getData(), 'id'))
                ->get()
                ->keyBy('directory_value_id');
            foreach ($response->getData() as $valuePim) {
                $directoryValue = $directoryValues->get($valuePim->getId());
                if (!$directoryValue) {
                    $directoryValue = new PropertyDirectoryValue();
                    $directoryValue->directory_value_id = $valuePim->getId();
                }
                $directoryValue->property_id = $valuePim->getPropertyId();
                $directoryValue->name = $valuePim->getName();
                $directoryValue->code = $valuePim->getCode();
                $directoryValue->value = $valuePim->getValue();
                $directoryValue->type = $valuePim->getType();
                $directoryValue->created_at = now();
                $directoryValue->updated_at = now();
                $directoryValue->is_migrated = true;
                $directoryValue->save();
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            if (!$nextCursor) {
                break;
            }

            $request->getPagination()->setCursor($nextCursor);
        }

        $this->executeAfterMigration([PropertyDirectoryValue::class]);
    }

    protected function loadProducts(): void
    {
        $this->executeBeforeMigration([Product::class, Image::class, ProductPropertyValue::class]);

        $request = new SearchProductsRequest();
        $request->setInclude(['images', 'attributes']);
        $request->setPagination((new PimRequestBodyPagination())->setLimit(50)->setType(PimPaginationTypeEnum::CURSOR));
        while (true) {
            $response = $this->productsApi->searchProducts($request);

            $productIds = Arr::pluck($response->getData(), 'id');
            $products = Product::query()->whereIn('product_id', $productIds)->get()->keyBy('product_id');
            $images = Image::query()->whereIn('product_id', $productIds)->get()->keyBy('image_id');
            $values = ProductPropertyValue::query()->whereIn('product_id', $productIds)->get()->keyBy('product_property_value_id');
            foreach ($response->getData() as $productPim) {
                $product = $products->get($productPim->getId());
                if (!$product) {
                    $product = new Product();
                    $product->product_id = $productPim->getId();
                }
                $product->allow_publish = $productPim->getAllowPublish();
                $product->main_image_file = $productPim->getMainImageFile()?->getPath();
                $product->category_id = $productPim->getCategoryId();
                $product->brand_id = $productPim->getBrandId();
                $product->name = $productPim->getName();
                $product->code = $productPim->getCode();
                $product->description = $productPim->getDescription();
                $product->type = $productPim->getType();
                $product->vendor_code = $productPim->getVendorCode();
                $product->barcode = $productPim->getBarcode();
                $product->weight = $productPim->getWeight();
                $product->weight_gross = $productPim->getWeightGross();
                $product->width = $productPim->getWidth();
                $product->height = $productPim->getHeight();
                $product->length = $productPim->getLength();
                $product->is_adult = $productPim->getIsAdult();
                $product->created_at = $productPim->getCreatedAt();
                $product->updated_at = $productPim->getUpdatedAt();
                $product->is_migrated = true;
                $product->save();

                foreach ($productPim->getImages() as $imagePim) {
                    $image = $images->get($imagePim->getId());
                    if (!$image) {
                        $image = new Image();
                        $image->image_id = $imagePim->getId();
                    }
                    $image->product_id = $productPim->getId();
                    $image->name = $imagePim->getName();
                    $image->sort = $imagePim->getSort();
                    $image->file = $imagePim->getFile()?->getPath();
                    $image->created_at = now();
                    $image->updated_at = now();
                    $image->is_migrated = true;
                    $image->save();
                }

                foreach ($productPim->getAttributes() as $valuePim) {
                    $value = $values->get($valuePim->getId());
                    if (!$value) {
                        $value = new ProductPropertyValue();
                        $value->product_property_value_id = $valuePim->getId();
                    }
                    $value->product_id = $productPim->getId();
                    $value->property_id = $valuePim->getPropertyId();
                    $value->directory_value_id = $valuePim->getDirectoryValueId();
                    $value->type = $valuePim->getType();
                    $value->value = $valuePim->getValue();
                    $value->name = $valuePim->getName();
                    $value->created_at = now();
                    $value->updated_at = now();
                    $value->is_migrated = true;
                    $value->save();
                }
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            if (!$nextCursor) {
                break;
            }

            $request->getPagination()->setCursor($nextCursor);
        }

        $this->executeAfterMigration([Product::class, Image::class, ProductPropertyValue::class]);
    }

    protected function loadOffers(): void
    {
        $this->executeBeforeMigration([Offer::class]);

        $request = new SearchOffersRequest();
        $request->setPagination((new OffersRequestBodyPagination())->setLimit(50)->setType(OffersPaginationTypeEnum::CURSOR));
        while (true) {
            $response = $this->offersApi->searchOffers($request);

            $offers = Offer::query()->whereIn('offer_id', Arr::pluck($response->getData(), 'id'))->get()->keyBy('offer_id');
            foreach ($response->getData() as $offerService) {
                $offer = $offers->get($offerService->getId());
                if (!$offer) {
                    $offer = new Offer();
                    $offer->offer_id = $offerService->getId();
                }
                $offer->product_id = $offerService->getProductId();
                $offer->base_price = $offerService->getBasePrice();
                $offer->is_migrated = true;
                $offer->save();
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            if (!$nextCursor) {
                break;
            }

            $request->getPagination()->setCursor($nextCursor);
        }

        $this->executeAfterMigration([Offer::class]);
    }

    protected function loadProductGroups(): void
    {
        $this->executeBeforeMigration([ProductGroup::class, ProductGroupProduct::class]);

        $request = new SearchProductGroupsRequest();
        $request->setInclude(['product_links']);
        $request->setPagination((new PimRequestBodyPagination())->setLimit(50)->setType(PimPaginationTypeEnum::CURSOR));
        while (true) {
            $response = $this->productGroupsApi->searchProductGroups($request);

            $productGroups = ProductGroup::query()
                ->whereIn('product_group_id', Arr::pluck($response->getData(), 'id'))
                ->get()
                ->keyBy('product_group_id');
            $productGroupProducts = ProductGroupProduct::query()
                ->whereIn(
                    'product_group_product_id',
                    array_filter(Arr::collapse(Arr::pluck($response->getData(), 'product_links.*.id')))
                )
                ->get()
                ->keyBy('product_group_product_id');
            foreach ($response->getData() as $productGroupPim) {
                $productGroup = $productGroups->get($productGroupPim->getId());
                if (!$productGroup) {
                    $productGroup = new ProductGroup();
                    $productGroup->product_group_id = $productGroupPim->getId();
                }
                $productGroup->category_id = $productGroupPim->getCategoryId();
                $productGroup->name = $productGroupPim->getName();
                $productGroup->main_product_id = $productGroupPim->getMainProductId();
                $productGroup->created_at = $productGroupPim->getCreatedAt();
                $productGroup->updated_at = $productGroupPim->getUpdatedAt();
                $productGroup->is_active = $productGroupPim->getIsActive();
                $productGroup->is_migrated = true;
                $productGroup->save();

                $productLinks = $productGroupPim->getProductLinks();
                if ($productLinks) {
                    foreach ($productLinks as $productLinkPim) {
                        $productGroupProduct = $productGroupProducts->get($productLinkPim->getId());
                        if (!$productGroupProduct) {
                            $productGroupProduct = new ProductGroupProduct();
                            $productGroupProduct->product_group_product_id = $productLinkPim->getId();
                        }
                        $productGroupProduct->product_group_id = $productLinkPim->getProductGroupId();
                        $productGroupProduct->product_id = $productLinkPim->getProductId();
                        $productGroupProduct->created_at = $productLinkPim->getCreatedAt();
                        $productGroupProduct->updated_at = $productLinkPim->getUpdatedAt();
                        $productGroupProduct->is_migrated = true;
                        $productGroupProduct->save();
                    }
                }
            }

            $nextCursor = $response->getMeta()->getPagination()->getNextCursor();
            if (!$nextCursor) {
                break;
            }

            $request->getPagination()->setCursor($nextCursor);
        }

        $this->executeAfterMigration([ProductGroup::class, ProductGroupProduct::class]);
    }
}
