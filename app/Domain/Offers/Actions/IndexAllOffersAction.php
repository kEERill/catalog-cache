<?php

namespace App\Domain\Offers\Actions;

use App\Domain\Offers\Models\Offer;

class IndexAllOffersAction
{
    public function __construct(private readonly IndexingOffersAction $indexOffers)
    {
    }

    public function execute(): void
    {
        Offer::queryForIndexing()
            ->chunkById(500, $this->indexOffers->execute(...));
    }
}
