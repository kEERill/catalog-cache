<?php

namespace App\Domain\Offers\Elastic;

use Ensi\LaravelElasticQuerySpecification\Filtering\AllowedFilter;
use Ensi\LaravelElasticQuerySpecification\Specification\CompositeSpecification;

class OfferSpecification extends CompositeSpecification
{
    public function __construct()
    {
        parent::__construct();

        $this->allowedFilters([
            'offer_id',
            'product_id',
            'allow_publish',
            'code',
            'category_id',
            'vendor_code',
            'barcode',
            'is_adult',
            'type',
            'brand_id',
            'gluing_is_main',
            'gluing_is_active',
            AllowedFilter::match('name', 'name'),
        ]);

        $this->allowedSorts([
            'product_id',
            'offer_id',
            'name_sort',
            'code',
        ]);
    }
}
