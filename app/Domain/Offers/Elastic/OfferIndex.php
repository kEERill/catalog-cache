<?php

namespace App\Domain\Offers\Elastic;

use App\Domain\Elastic\Concerns\ElasticIndex;

class OfferIndex extends ElasticIndex
{
    protected string $name = 'offers';
    protected string $tiebreaker = 'offer_id';

    protected function settings(): array
    {
        return [
            'settings' => [
                #'number_of_shards' => 1,
                #'index.number_of_replicas' => '0',
                'index.max_result_window' => 500000,
                'index.max_inner_result_window' => 100000,
                'analysis' => [
                    'filter' => [
                        'russian_stop' => [
                            'type' => 'stop',
                            'stopwords' => '_russian_',
                        ],
                        'russian_stemmer' => [
                            'type' => 'stemmer',
                            'language' => 'russian',
                        ],
                    ],
                    'analyzer' => [
                        'default' => [
                            'type' => 'custom',
                            'char_filter' => ['html_strip'],
                            'tokenizer' => 'standard',
                            'filter' => [
                                'lowercase',
                                'russian_stop',
                                'russian_stemmer',
                            ],
                        ],
                    ],
                    'normalizer' => [
                        'keyword_lowercase' => [
                            'type' => 'custom',
                            'filter' => ['lowercase'],
                        ],
                    ],
                ],
            ],
            'mappings' => [
                'properties' => [
                    "offer_id" => ['type' => 'integer'],
                    "product_id" => ['type' => 'integer'],

                    // Свойства товара
                    "allow_publish" => ['type' => 'boolean'],
                    "main_image_file" => ['type' => 'keyword', 'index' => false],
                    "category_id" => ['type' => 'keyword'],
                    "brand_id" => ['type' => 'keyword'],
                    "name" => ['type' => 'text'],
                    "name_sort" => ['type' => 'keyword', 'normalizer' => 'keyword_lowercase'],
                    "code" => ['type' => 'keyword'],
                    "description" => ['type' => 'text'],
                    "type" => ['type' => 'keyword'],
                    "vendor_code" => ['type' => 'keyword'],
                    "barcode" => ['type' => 'keyword'],
                    "weight" => ['type' => 'float', 'index' => false],
                    "weight_gross" => ['type' => 'float', 'index' => false],
                    "length" => ['type' => 'float', 'index' => false],
                    "width" => ['type' => 'float', 'index' => false],
                    "height" => ['type' => 'float', 'index' => false],
                    "is_adult" => ['type' => 'boolean'],

                    // Свойства из офера
                    "cost" => ['type' => 'long'],
                    "price" => ['type' => 'long'],

                    // Зависимости
                    "discount" => [
                        'type' => 'object',
                        'properties' => [
                            "value_type" => ['type' => 'integer'],
                            "value" => ['type' => 'integer'],
                        ],
                    ],
                    "brand" => [
                        'type' => 'object',
                        'properties' => [
                            "id" => ['type' => 'keyword'],
                            "name" => ['type' => 'text'],
                            "code" => ['type' => 'keyword'],
                            "description" => ['type' => 'text'],
                            "logo_file" => ['type' => 'keyword', 'index' => false],
                            "logo_url" => ['type' => 'keyword', 'index' => false],
                        ],
                    ],
                    "category" => [
                        'type' => 'object',
                        'properties' => [
                            "id" => ['type' => 'keyword'],
                            "name" => ['type' => 'text'],
                            "code" => ['type' => 'keyword'],
                            "parent_id" => ['type' => 'keyword'],
                        ],
                    ],
                    "images" => [
                        'type' => 'object',
                        'properties' => [
                            "id" => ['type' => 'keyword'],
                            "url" => ['type' => 'keyword', 'index' => false],
                            "name" => ['type' => 'keyword'],
                            "sort" => ['type' => 'integer'],
                        ],
                    ],
                    "props" => [
                        'type' => 'object',
                        'properties' => [
                            "type" => ['type' => 'keyword'],
                            "name" => ['type' => 'keyword'],
                            "code" => ['type' => 'keyword'],
                            "values" => [
                                'type' => 'object',
                                'properties' => [
                                    "value" => ['type' => 'keyword'],
                                    "name" => ['type' => 'keyword'],
                                ],
                            ],
                        ],
                    ],
                    "nameplates" => [
                        'type' => 'object',
                        'properties' => [
                            "id" => ['type' => 'keyword'],
                            "name" => ['type' => 'text'],
                            "code" => ['type' => 'keyword'],
                            "background_color" => ['type' => 'keyword', 'index' => false],
                            "text_color" => ['type' => 'keyword', 'index' => false],
                        ],
                    ],
                    "gluing_name" => ['type' => 'keyword'],
                    "gluing_is_main" => ['type' => 'boolean'],
                    "gluing_is_active" => ['type' => 'boolean'],
                    "gluing" => [
                        'type' => 'object',
                        'properties' => [
                            "id" => ['type' => 'keyword'],
                            "name" => ['type' => 'keyword'],
                            "code" => ['type' => 'keyword'],
                            "props" => [
                                'type' => 'object',
                                'properties' => [
                                    "prop_type" => ['type' => 'keyword'],
                                    "prop_name" => ['type' => 'keyword'],
                                    "prop_code" => ['type' => 'keyword'],
                                    "value_value" => ['type' => 'keyword'],
                                    "value_name" => ['type' => 'keyword'],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }
}
