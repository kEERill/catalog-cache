<?php

namespace App\Domain\Offers\Elastic\Data;

use App\Domain\Elastic\Data\ElasticModel;
use App\Domain\Offers\Elastic\Data\Tests\Factories\OfferFactory;

/**
 * @property int $offer_id Id оффера из Offers
 * @property int $product_id Id товара из PIM
 * @property bool $allow_publish Товар разрешен для публикации
 * @property string|null $main_image_file Файл основной картинки
 * @property int $category_id Id категории из PIM
 * @property int|null $brand_id Id бренда из PIM
 * @property string $name Название (для полготекстового поиска)
 * @property string $name_sort Название (для сортировки)
 * @property string $code ЧПУ код
 * @property string|null $description Описание
 * @property int $type Тип товара (весовой, штучный, ...) из перечисления ProductTypeEnum
 * @property string $vendor_code Артикул
 * @property string|null $barcode Штрихкод (EAN)
 * @property float|null $weight Вес нетто (кг)
 * @property float|null $weight_gross Вес брутто (кг)
 * @property float|null $width Ширина (мм)
 * @property float|null $height Высота (мм)
 * @property float|null $length Длина (мм)
 * @property bool $is_adult Товар 18+
 * @property int|null $cost - Цена до скидки
 * @property int|null $price Итоговая цена
 * @property DiscountData|null $discount Скидка
 * @property BrandData|null $brand Бренд товара
 * @property CategoryData $category Категория товара
 * @property ImageData[] $images Изображения товара
 * @property PropertyData[] $props Свойства товара
 * @property string|null $gluing_name Название товарной склейки
 * @property bool|null $gluing_is_main Товар является главным в склейке
 * @property bool $gluing_is_active Активность товарной склейки
 * @property GluingData[] $gluing Товары из склейки
 * @property NameplateData[] $nameplates Товарные теги
 */
class OfferData extends ElasticModel
{
    public function __construct($attributes = [])
    {
        $attributes['brand'] = isset($attributes['brand']) ? new BrandData($attributes['brand']) : null;
        $attributes['category'] = isset($attributes['category']) ? new CategoryData($attributes['category']) : null;
        $attributes['discount'] = isset($attributes['discount']) ? new DiscountData($attributes['discount']) : null;

        $images = [];
        foreach ($attributes['images'] ?? [] as $image) {
            $images[] = new ImageData($image);
        }
        $attributes['images'] = $images;

        $properties = [];
        foreach ($attributes['props'] ?? [] as $property) {
            $properties[] = new PropertyData($property);
        }
        $attributes['props'] = $properties;

        $gluing = [];
        foreach ($attributes['gluing'] ?? [] as $data) {
            $gluing[] = new GluingData($data);
        }
        $attributes['gluing'] = $gluing;

        $nameplates = [];
        foreach ($attributes['nameplates'] ?? [] as $nameplate) {
            $nameplates[] = new NameplateData($nameplate);
        }
        $attributes['nameplates'] = $nameplates;

        parent::__construct($attributes);
    }

    public static function factory(): OfferFactory
    {
        return OfferFactory::new();
    }

    public function getId(): string|int
    {
        return $this->offer_id;
    }
}
