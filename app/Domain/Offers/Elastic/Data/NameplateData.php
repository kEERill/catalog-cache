<?php

namespace App\Domain\Offers\Elastic\Data;

use App\Domain\Offers\Elastic\Data\Tests\Factories\NameplateFactory;
use Illuminate\Support\Fluent;

/**
 * @property int $id
 * @property string $name - наименование тега
 * @property string $code - код тега
 * @property string $background_color - цвет фона
 * @property string $text_color - цвет текста
 */
class NameplateData extends Fluent
{
    public static function factory(): NameplateFactory
    {
        return NameplateFactory::new();
    }
}
