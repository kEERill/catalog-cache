<?php

namespace App\Domain\Offers\Elastic\Data;

use App\Domain\Offers\Elastic\Data\Tests\Factories\ImageFactory;
use Illuminate\Support\Fluent;

/**
 * @property int $id ID изображения из PIM
 * @property string|null $name Описание картинки
 * @property int $sort Порядок сортировки
 * @property string $url URL файла
 */
class ImageData extends Fluent
{
    public static function factory(): ImageFactory
    {
        return ImageFactory::new();
    }
}
