<?php

namespace App\Domain\Offers\Elastic\Data;

use App\Domain\Offers\Elastic\Data\Tests\Factories\DiscountFactory;
use Illuminate\Support\Fluent;

/**
 * @property int $value_type - тип значения
 * @property int $value - размер скидки
 */
class DiscountData extends Fluent
{
    public static function factory(): DiscountFactory
    {
        return DiscountFactory::new();
    }
}
