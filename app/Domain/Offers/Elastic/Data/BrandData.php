<?php

namespace App\Domain\Offers\Elastic\Data;

use App\Domain\Offers\Elastic\Data\Tests\Factories\BrandFactory;
use Illuminate\Support\Fluent;

/**
 * @property int $id ID бренда из PIM
 * @property string $name Название
 * @property string $code ЧПУ код
 * @property string|null $description Описание бренда
 * @property string|null $logo_file Файл логотипа
 * @property string|null $logo_url URL логотипа
 */
class BrandData extends Fluent
{
    public static function factory(): BrandFactory
    {
        return BrandFactory::new();
    }
}
