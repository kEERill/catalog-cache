<?php

namespace App\Domain\Offers\Elastic\Data\Tests\Factories;

use App\Domain\Offers\Elastic\Data\GluingData;
use App\Domain\Offers\Elastic\Data\GluingPropertyData;
use Ensi\LaravelTestFactories\BaseApiFactory;

class GluingFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $props = [];
        for ($i = 0; $i <= $this->faker->numberBetween(1, 2); $i++) {
            $props[] = GluingPropertyData::factory()->makeArray();
        }

        return [
            'id' => $this->faker->modelId(),
            'name' => $this->faker->sentence,
            'code' => $this->faker->slug,
            'props' => $props,
        ];
    }

    public function make(array $extra = []): GluingData
    {
        return new GluingData($this->makeArray($extra));
    }
}
