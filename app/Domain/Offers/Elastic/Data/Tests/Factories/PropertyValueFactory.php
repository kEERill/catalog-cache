<?php

namespace App\Domain\Offers\Elastic\Data\Tests\Factories;

use App\Domain\Offers\Elastic\Data\PropertyValueData;
use Ensi\LaravelTestFactories\BaseApiFactory;

class PropertyValueFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->nullable()->sentence,
            'value' => $this->faker->word,
        ];
    }

    public function make(array $extra = []): PropertyValueData
    {
        return new PropertyValueData($this->makeArray($extra));
    }
}
