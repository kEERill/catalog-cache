<?php

namespace App\Domain\Offers\Elastic\Data\Tests\Factories;

use App\Domain\Offers\Elastic\Data\CategoryData;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CategoryFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'code' => $this->faker->slug,
            'name' => $this->faker->company,
            'parent_id' => $this->faker->nullable()->modelId(),
        ];
    }

    public function make(array $extra = []): CategoryData
    {
        return new CategoryData($this->makeArray($extra));
    }
}
