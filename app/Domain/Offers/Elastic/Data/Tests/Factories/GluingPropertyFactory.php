<?php

namespace App\Domain\Offers\Elastic\Data\Tests\Factories;

use App\Domain\Offers\Elastic\Data\GluingPropertyData;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class GluingPropertyFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'prop_type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'prop_name' => $this->faker->sentence,
            'prop_code' => $this->faker->slug,
            'value_value' => $this->faker->word,
            'value_name' => $this->faker->nullable()->sentence,
        ];
    }

    public function make(array $extra = []): GluingPropertyData
    {
        return new GluingPropertyData($this->makeArray($extra));
    }
}
