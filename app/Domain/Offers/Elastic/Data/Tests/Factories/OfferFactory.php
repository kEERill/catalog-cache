<?php

namespace App\Domain\Offers\Elastic\Data\Tests\Factories;

use App\Domain\Elastic\Concerns\ElasticIndex;
use App\Domain\Offers\Actions\MakeOfferElasticFromModelAction;
use App\Domain\Offers\Elastic\Data\BrandData;
use App\Domain\Offers\Elastic\Data\CategoryData;
use App\Domain\Offers\Elastic\Data\DiscountData;
use App\Domain\Offers\Elastic\Data\GluingData;
use App\Domain\Offers\Elastic\Data\ImageData;
use App\Domain\Offers\Elastic\Data\NameplateData;
use App\Domain\Offers\Elastic\Data\OfferData;
use App\Domain\Offers\Elastic\Data\PropertyData;
use App\Domain\Offers\Elastic\OfferIndex;
use App\Domain\Offers\Models\Offer;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Tests\Factories\BaseElasticFactory;

class OfferFactory extends BaseElasticFactory
{
    protected bool $withGluing = false;
    protected bool $withImages = false;
    protected bool $withProps = false;
    protected bool $withNameplates = false;

    protected function definition(): array
    {
        $hasGluing = $this->withGluing ?: $this->faker->boolean;
        $gluing = [];
        if ($hasGluing) {
            for ($i = 0; $i <= $this->faker->numberBetween(0, 3); $i++) {
                $gluing[] = GluingData::factory()->makeArray();
            }
        }

        return [
            'offer_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'allow_publish' => $this->faker->boolean,
            'main_image_file' => $this->faker->filePath(),
            'category_id' => $this->faker->modelId(),
            'brand_id' => $this->faker->modelId(),
            'name' => $this->faker->sentence(),
            'code' => $this->faker->slug(),
            'description' => $this->faker->nullable()->text,
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'vendor_code' => $this->faker->numerify('###-###-###'),
            'barcode' => $this->faker->nullable()->ean13(),
            'weight' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'length' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'height' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'width' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,
            'price' => $this->faker->nullable()->randomNumber(),
            'discount' => DiscountData::factory()->makeArray(),
            'brand' => BrandData::factory()->makeArray(),
            'category' => CategoryData::factory()->makeArray(),
            'nameplates' => $this->faker->randomList(fn () => NameplateData::factory()->makeArray(), $this->withNameplates ? 1 : 0, 3),
            'images' => $this->faker->randomList(fn () => ImageData::factory()->makeArray(), $this->withImages ? 1 : 0, 3),
            'props' => $this->faker->randomList(fn () => PropertyData::factory()->makeArray(), $this->withProps ? 1 : 0, 3),
            'gluing_name' => $hasGluing ? $this->faker->sentence() : null,
            'gluing_is_main' => $hasGluing ? $this->faker->boolean : null,
            'gluing_is_active' => $hasGluing ? $this->faker->boolean : null,
            'gluing' => $gluing,
        ];
    }

    public function withAllRelations(): self
    {
        return $this
            ->withGluing()
            ->withImages()
            ->withProps()
            ->withNameplates();
    }

    public function withGluing(): self
    {
        $this->withGluing = true;

        return $this;
    }

    public function withImages(): self
    {
        $this->withImages = true;

        return $this;
    }

    public function withProps(): self
    {
        $this->withProps = true;

        return $this;
    }

    public function withNameplates(): self
    {
        $this->withNameplates = true;

        return $this;
    }

    public function fromModel(Offer $model): self
    {
        $data = resolve(MakeOfferElasticFromModelAction::class)->execute($model);

        if (!$data) {
            return $this->state([
                'offer_id' => $model->offer_id,
                'product_id' => $model->product_id,
                'price' => $model->base_price,
            ]);
        }

        return $this->state(json_decode($data->toJson(), true));
    }

    public function make(array $extra = []): OfferData
    {
        return new OfferData($this->makeArray($extra));
    }

    protected function index(): ElasticIndex
    {
        return new OfferIndex();
    }
}
