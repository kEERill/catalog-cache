<?php

namespace App\Domain\Offers\Elastic\Data\Tests\Factories;

use App\Domain\Offers\Elastic\Data\BrandData;
use Ensi\LaravelTestFactories\BaseApiFactory;

class BrandFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $logoFile = $this->faker->nullable()->filePath();

        return [
            'id' => $this->faker->modelId(),
            'code' => $this->faker->slug,
            'name' => $this->faker->company,
            'description' => $this->faker->nullable()->sentence,
            'logo_file' => $logoFile,
            'logo_url' => $this->faker->nullable($logoFile ? 1 : 0)->imageUrl,
        ];
    }

    public function make(array $extra = []): BrandData
    {
        return new BrandData($this->makeArray($extra));
    }
}
