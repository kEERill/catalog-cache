<?php

namespace App\Domain\Offers\Elastic\Data\Tests\Factories;

use App\Domain\Offers\Elastic\Data\PropertyData;
use App\Domain\Offers\Elastic\Data\PropertyValueData;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\PimClient\Dto\PropertyTypeEnum;

class PropertyFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $values = [];
        for ($i = 0; $i <= $this->faker->numberBetween(1, 2); $i++) {
            $values[] = PropertyValueData::factory()->makeArray();
        }

        return [
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'name' => $this->faker->sentence,
            'code' => $this->faker->slug,
            'values' => $values,
        ];
    }

    public function make(array $extra = []): PropertyData
    {
        return new PropertyData($this->makeArray($extra));
    }
}
