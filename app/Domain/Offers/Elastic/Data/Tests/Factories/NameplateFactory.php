<?php

namespace App\Domain\Offers\Elastic\Data\Tests\Factories;

use App\Domain\Offers\Elastic\Data\NameplateData;
use Ensi\LaravelTestFactories\BaseApiFactory;

class NameplateFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->unique()->modelId(),
            'name' => $this->faker->text(50),
            'code' => $this->faker->unique()->text(50),
            'background_color' => $this->faker->hexColor(),
            'text_color' => $this->faker->hexColor(),
        ];
    }

    public function make(array $extra = []): NameplateData
    {
        return new NameplateData($this->makeArray($extra));
    }
}
