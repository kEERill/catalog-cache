<?php

namespace App\Domain\Offers\Elastic\Data;

use App\Domain\Offers\Elastic\Data\Tests\Factories\CategoryFactory;
use Illuminate\Support\Fluent;

/**
 * @property int $id ID категории из PIM
 * @property string $name Название
 * @property string $code ЧПУ код категории
 * @property int|null $parent_id Id родительской категории из PIM
 */
class CategoryData extends Fluent
{
    public static function factory(): CategoryFactory
    {
        return CategoryFactory::new();
    }
}
