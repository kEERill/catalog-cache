<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\Property;
use Illuminate\Database\Eloquent\Builder;

class PropertyObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(Property $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereHas(
            'product',
            fn (Builder $queryProduct) => $queryProduct->whereHas(
                'productPropertyValues',
                fn (Builder $queryPropertyValue) => $queryPropertyValue->where(
                    'property_id',
                    $model->property_id
                ),
            ),
        ));
    }

    // На deleted не вешаем $markAction, т.к. в таком случае придёт удаление по ProductPropertyValue,
    // и через него будет пометка на обновление
}
