<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\DeleteOfferFromIndexAction;
use App\Domain\Offers\Models\Offer;

class OfferObserver
{
    public function __construct(
        protected DeleteOfferFromIndexAction $deleteElasticAction
    ) {
    }

    public function deleted(Offer $model): void
    {
        $this->deleteElasticAction->execute($model->offer_id);
    }
}
