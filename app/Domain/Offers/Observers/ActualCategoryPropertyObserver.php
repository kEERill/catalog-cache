<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\ActualCategoryProperty;
use Illuminate\Database\Eloquent\Builder;

class ActualCategoryPropertyObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(ActualCategoryProperty $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereHas(
            'product',
            fn (Builder $queryProduct) => $queryProduct->whereHas(
                'productPropertyValues',
                fn (Builder $queryPropertyValue) => $queryPropertyValue->where(
                    'property_id',
                    $model->property_id
                ),
            ),
        ));
    }

    // На deleted не вешаем $markAction, т.к. в таком случае придёт обновление по самому товару,
    // и через него будет пометка на обновление
}
