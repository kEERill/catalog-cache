<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\ProductGroupProduct;
use Illuminate\Database\Eloquent\Builder;

class ProductGroupProductObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(ProductGroupProduct $model): void
    {
        $this->mark($model);
    }

    public function deleted(ProductGroupProduct $model): void
    {
        $this->mark($model);
    }

    protected function mark(ProductGroupProduct $model): void
    {
        $model->loadMissing('productGroup.productLinks');
        $productsInProductGroup = collect([$model->product_id]);
        if ($model->productGroup) {
            $productsInProductGroup = $productsInProductGroup->merge($model->productGroup->productLinks->pluck('product_id'))->unique();
        }
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereIn('product_id', $productsInProductGroup));
    }
}
