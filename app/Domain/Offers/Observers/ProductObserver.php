<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\Product;
use Illuminate\Database\Eloquent\Builder;

class ProductObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(Product $model): void
    {
        $this->mark($model);
    }

    public function deleted(Product $model): void
    {
        $this->mark($model);
    }

    protected function mark(Product $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->where('product_id', $model->product_id));
    }
}
