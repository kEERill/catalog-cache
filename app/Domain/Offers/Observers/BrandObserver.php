<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\Brand;
use Illuminate\Database\Eloquent\Builder;

class BrandObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(Brand $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereHas(
            'product',
            fn (Builder $queryProduct) => $queryProduct->where('brand_id', $model->brand_id),
        ));
    }

    // На deleted не вешаем $markAction, т.к. в таком случае придёт обновление по самому товару,
    // и через него будет пометка на обновление
}
