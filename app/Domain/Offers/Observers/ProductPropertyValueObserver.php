<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\ProductPropertyValue;
use Illuminate\Database\Eloquent\Builder;

class ProductPropertyValueObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(ProductPropertyValue $model): void
    {
        $this->mark($model);
    }

    public function deleted(ProductPropertyValue $model): void
    {
        $this->mark($model);
    }

    protected function mark(ProductPropertyValue $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->where('product_id', $model->product_id));
    }
}
