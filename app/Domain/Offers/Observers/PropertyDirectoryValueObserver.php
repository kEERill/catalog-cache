<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\PropertyDirectoryValue;
use Illuminate\Database\Eloquent\Builder;

class PropertyDirectoryValueObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(PropertyDirectoryValue $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereHas(
            'product',
            fn (Builder $queryProduct) => $queryProduct->whereHas(
                'productPropertyValues',
                fn (Builder $queryPropertyValue) => $queryPropertyValue->where(
                    'directory_value_id',
                    $model->directory_value_id
                ),
            ),
        ));
    }

    // На deleted не вешаем $markAction, т.к. в таком случае придёт обновление по ProductPropertyValue,
    // и через него будет пометка на обновление
}
