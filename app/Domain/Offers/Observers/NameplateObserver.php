<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\Nameplate;
use Illuminate\Database\Eloquent\Builder;

class NameplateObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(Nameplate $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereHas(
            'product',
            fn (Builder $queryProduct) => $queryProduct->whereHas(
                'nameplates',
                fn (Builder $queryNameplate) => $queryNameplate->where(
                    'nameplates.nameplate_id',
                    $model->nameplate_id
                ),
            ),
        ));
    }

    // На deleted не вешаем $markAction, т.к. в таком случае придёт удаление по NameplateProduct,
    // и через него будет пометка на обновление
}
