<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\Image;
use Illuminate\Database\Eloquent\Builder;

class ImageObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(Image $model): void
    {
        $this->mark($model);
    }

    public function deleted(Image $model): void
    {
        $this->mark($model);
    }

    protected function mark(Image $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->where('product_id', $model->product_id));
    }
}
