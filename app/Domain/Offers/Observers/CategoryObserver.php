<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\Category;
use Illuminate\Database\Eloquent\Builder;

class CategoryObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(Category $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereHas(
            'product',
            fn (Builder $queryProduct) => $queryProduct->where('category_id', $model->category_id),
        ));
    }

    // На deleted не вешаем $markAction, т.к. в таком случае придёт обновление по самому товару,
    // и через него будет пометка на обновление
}
