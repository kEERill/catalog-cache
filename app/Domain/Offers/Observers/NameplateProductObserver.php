<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\NameplateProduct;
use Illuminate\Database\Eloquent\Builder;

class NameplateProductObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(NameplateProduct $model): void
    {
        $this->mark($model);
    }

    public function deleted(NameplateProduct $model): void
    {
        $this->mark($model);
    }

    protected function mark(NameplateProduct $model): void
    {
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->where('product_id', $model->product_id));
    }
}
