<?php

namespace App\Domain\Offers\Observers;

use App\Domain\Offers\Actions\MarkOfferToIndexingAction;
use App\Domain\Offers\Models\ProductGroup;
use Illuminate\Database\Eloquent\Builder;

class ProductGroupObserver
{
    public function __construct(
        protected MarkOfferToIndexingAction $markAction
    ) {
    }

    public function saved(ProductGroup $model): void
    {
        $this->mark($model);
    }

    public function deleted(ProductGroup $model): void
    {
        $this->mark($model);
    }

    protected function mark(ProductGroup $model): void
    {
        $model->loadMissing('productLinks');
        $productsInProductGroup = $model->productLinks->pluck('product_id')->merge([$model->main_product_id])->unique();
        $this->markAction->execute(fn (Builder $queryOffer) => $queryOffer->whereIn('product_id', $productsInProductGroup));
    }
}
