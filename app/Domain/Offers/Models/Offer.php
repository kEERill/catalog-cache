<?php

namespace App\Domain\Offers\Models;

use App\Domain\Discounts\Models\Discount;
use App\Domain\Offers\Models\Tests\Factories\OfferFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property int $id
 *
 * @property int $offer_id - id оффера из Offers
 *
 * @property int $product_id - id товара из PIM
 *
 * @property int|null $base_price - базовая цена
 * @property int|null $price - итоговая цена со скидками. Если null, то рассчитается при индексации.
 *
 * @property CarbonInterface|null $created_at - дата создания (в текущем сервисе, а не внешнем)
 * @property CarbonInterface|null $updated_at - дата обновления (в текущем сервисе, а не внешнем)
 *
 * @property Product|null $product
 * @property Discount|null $discount
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class Offer extends Model
{
    protected $table = 'offers';

    public static function factory(): OfferFactory
    {
        return OfferFactory::new();
    }

    public function product(): BelongsTo
    {
        return $this->belongsTo(Product::class, 'product_id', 'product_id');
    }

    public function discount(): HasOne
    {
        return $this->hasOne(Discount::class, 'offer_id', 'offer_id');
    }

    public function notMark(): void
    {
        $this->timestamps = false;
    }

    public static function queryForIndexing(): Builder
    {
        return Offer::query()
            ->with([
                'discount',
                'product.brand',
                'product.category.actualProperties',
                'product.images',
                'product.productPropertyValues.property',
                'product.productPropertyValues.directoryValue',
                'product.productGroup.products.productPropertyValues.property',
                'product.productGroup.products.productPropertyValues.directoryValue',
                'product.productGroup.products.offers',
                'product.nameplates',
            ]);
    }
}
