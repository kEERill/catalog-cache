<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\CategoryFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 *
 * @property int $category_id ID категории из PIM
 *
 * @property string $name Название
 * @property string $code ЧПУ код категории
 * @property int|null $parent_id Id родительской категории из PIM
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property-read Collection|ActualCategoryProperty[] $actualProperties Привязка атрибутов к категории
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class Category extends Model
{
    protected $table = 'categories';

    public function actualProperties(): HasMany
    {
        return $this->hasMany(ActualCategoryProperty::class, 'category_id', 'category_id');
    }

    public static function factory(): CategoryFactory
    {
        return CategoryFactory::new();
    }
}
