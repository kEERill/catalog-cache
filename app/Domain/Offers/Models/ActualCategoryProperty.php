<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\ActualCategoryPropertyFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property int $actual_category_property_id Id связи из PIM
 *
 * @property int $property_id Id свойства из PIM
 * @property int $category_id Id категории из PIM
 * @property bool $is_gluing Свойство используется для склейки товаров
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class ActualCategoryProperty extends Model
{
    protected $table = 'actual_category_properties';

    protected $casts = [
        'property_id' => 'int',
        'category_id' => 'int',
        'is_gluing' => 'bool',
    ];

    public static function factory(): ActualCategoryPropertyFactory
    {
        return ActualCategoryPropertyFactory::new();
    }
}
