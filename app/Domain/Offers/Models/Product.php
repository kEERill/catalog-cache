<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\ProductFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;

/**
 * @property int $id
 *
 * @property int $product_id - id товара из PIM
 *
 * @property bool $allow_publish Разрешена ли публикация
 * @property string|null $main_image_file Файл основной картинки
 * @property int $category_id Id категории из PIM
 * @property int|null $brand_id Id бренда из PIM
 * @property string $name Название
 * @property string $code ЧПУ код
 * @property string|null $description Описание
 * @property int $type Тип товара (весовой, штучный, ...) из перечисления ProductTypeEnum
 * @property string $vendor_code Артикул
 * @property string|null $barcode Штрихкод (EAN)
 * @property float|null $weight Вес нетто (кг)
 * @property float|null $weight_gross Вес брутто (кг)
 * @property float|null $width Ширина (мм)
 * @property float|null $height Высота (мм)
 * @property float|null $length Длина (мм)
 * @property bool $is_adult Товар 18+
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property Brand|null $brand
 * @property Category|null $category
 * @property Collection|Image[] $images
 * @property Collection|ProductPropertyValue[] $productPropertyValues
 * @property Collection|Nameplate[] $nameplates - Товарные теги
 * @property-read ProductGroup|null $productGroup Товарная склейка, в которую входит товар
 * @property-read Collection|Offer[] $offers Предложения товара
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class Product extends Model
{
    protected $table = 'products';

    protected $casts = [
        'weight' => 'float',
        'weight_gross' => 'float',
        'width' => 'float',
        'height' => 'float',
        'length' => 'float',
        'category_id' => 'int',
        'brand_id' => 'int',
        'is_adult' => 'bool',
        'allow_publish' => 'bool',
    ];

    public static function factory(): ProductFactory
    {
        return ProductFactory::new();
    }

    public function productPropertyValues(): HasMany
    {
        return $this->hasMany(ProductPropertyValue::class, 'product_id', 'product_id');
    }

    public function images(): HasMany
    {
        return $this->hasMany(Image::class, 'product_id', 'product_id');
    }

    public function brand(): BelongsTo
    {
        return $this->belongsTo(Brand::class, 'brand_id', 'brand_id');
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class, 'category_id', 'category_id');
    }

    public function nameplates(): HasManyThrough
    {
        return $this->hasManyThrough(
            Nameplate::class,
            NameplateProduct::class,
            'product_id',
            'nameplate_id',
            'product_id',
            'nameplate_id',
        );
    }

    public function productGroup(): HasOneThrough
    {
        //По ФЗ товар может состоять только в одной склейке
        return $this->hasOneThrough(
            ProductGroup::class,
            ProductGroupProduct::class,
            'product_id',
            'product_group_id',
            'product_id',
            'product_group_id'
        );
    }

    public function offers(): HasMany
    {
        return $this->hasMany(Offer::class, 'product_id', 'product_id');
    }
}
