<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Product;
use Ensi\PimClient\Dto\ProductTypeEnum;
use Tests\Factories\BaseModelFactory;

class ProductFactory extends BaseModelFactory
{
    protected $model = Product::class;

    public function definition(): array
    {
        return [
            'product_id' => $this->faker->modelId(),
            'allow_publish' => $this->faker->boolean,
            'main_image_file' => $this->faker->filePath(),
            'category_id' => $this->faker->modelId(),
            'brand_id' => $this->faker->nullable()->modelId(),
            'name' => $this->faker->sentence(),
            'code' => $this->faker->slug(),
            'description' => $this->faker->nullable()->text,
            'type' => $this->faker->randomElement(ProductTypeEnum::getAllowableEnumValues()),
            'vendor_code' => $this->faker->numerify('###-###-###'),
            'barcode' => $this->faker->nullable()->ean13(),
            'weight' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'weight_gross' => $this->faker->nullable()->randomFloat(3, 1, 100),
            'length' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'height' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'width' => $this->faker->nullable()->randomFloat(2, 1, 1000),
            'is_adult' => $this->faker->boolean,
        ];
    }
}
