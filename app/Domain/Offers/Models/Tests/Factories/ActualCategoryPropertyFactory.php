<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\ActualCategoryProperty;
use Tests\Factories\BaseModelFactory;

class ActualCategoryPropertyFactory extends BaseModelFactory
{
    protected $model = ActualCategoryProperty::class;

    public function definition(): array
    {
        return [
            'actual_category_property_id' => $this->faker->modelId(),
            'property_id' => $this->faker->modelId(),
            'category_id' => $this->faker->modelId(),
            'is_gluing' => $this->faker->boolean,
        ];
    }
}
