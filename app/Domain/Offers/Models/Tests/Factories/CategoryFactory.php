<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Category;
use Tests\Factories\BaseModelFactory;

class CategoryFactory extends BaseModelFactory
{
    protected $model = Category::class;

    public function definition(): array
    {
        return [
            'category_id' => $this->faker->modelId(),
            'code' => $this->faker->slug,
            'name' => $this->faker->company,
            'parent_id' => $this->faker->nullable()->modelId(),
        ];
    }
}
