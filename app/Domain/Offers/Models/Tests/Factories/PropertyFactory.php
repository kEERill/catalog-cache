<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Property;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Tests\Factories\BaseModelFactory;

class PropertyFactory extends BaseModelFactory
{
    protected $model = Property::class;

    public function definition(): array
    {
        return [
            'property_id' => $this->faker->modelId(),
            'name' => $this->faker->sentence,
            'code' => $this->faker->slug,
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'is_public' => $this->faker->boolean,
            'is_active' => $this->faker->boolean,
        ];
    }
}
