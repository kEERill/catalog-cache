<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\Brand;
use Tests\Factories\BaseModelFactory;

class BrandFactory extends BaseModelFactory
{
    protected $model = Brand::class;

    public function definition(): array
    {
        $logoFile = $this->faker->nullable()->filePath();

        return [
            'brand_id' => $this->faker->modelId(),
            'code' => $this->faker->slug,
            'name' => $this->faker->company,
            'description' => $this->faker->nullable()->sentence,
            'logo_file' => $logoFile,
            'logo_url' => $this->faker->nullable($logoFile ? 1 : 0)->imageUrl,
        ];
    }
}
