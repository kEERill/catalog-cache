<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\ProductPropertyValue;
use Ensi\PimClient\Dto\PropertyTypeEnum;
use Tests\Factories\BaseModelFactory;

class ProductPropertyValueFactory extends BaseModelFactory
{
    protected $model = ProductPropertyValue::class;
    protected ?bool $simple = null;

    public function definition(): array
    {
        $directoryValueId = $this->faker->nullable()->modelId();

        return [
            'product_property_value_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
            'property_id' => $this->faker->modelId(),
            'type' => $this->faker->randomElement(PropertyTypeEnum::getAllowableEnumValues()),
            'directory_value_id' => $directoryValueId,
            'value' => $this->faker->nullable($directoryValueId ? 0 : 1)->word, // Тут можно написать более умный генератор, но пока незачем
            'name' => $this->faker->nullable($directoryValueId ? 0 : 0.5)->sentence,
        ];
    }

    public function simple(): static
    {
        return $this->state([
            'directory_value_id' => null,
            'value' => $this->faker->word, // Тут можно написать более умный генератор, но пока незачем
            'name' => $this->faker->nullable()->sentence,
        ]);
    }

    public function directoryValueId(?int $directoryValueId = null): static
    {
        return $this->state([
            'directory_value_id' => $directoryValueId ?: $this->faker->modelId(),
            'value' => null,
            'name' => null,
        ]);
    }
}
