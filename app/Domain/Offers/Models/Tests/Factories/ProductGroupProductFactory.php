<?php

namespace App\Domain\Offers\Models\Tests\Factories;

use App\Domain\Offers\Models\ProductGroupProduct;
use Tests\Factories\BaseModelFactory;

class ProductGroupProductFactory extends BaseModelFactory
{
    protected $model = ProductGroupProduct::class;

    public function definition(): array
    {
        return [
            'product_group_product_id' => $this->faker->modelId(),
            'product_group_id' => $this->faker->modelId(),
            'product_id' => $this->faker->modelId(),
        ];
    }
}
