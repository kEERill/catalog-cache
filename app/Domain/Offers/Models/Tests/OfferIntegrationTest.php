<?php

use App\Domain\Offers\Elastic\Data\OfferData;
use App\Domain\Offers\Elastic\OfferIndex;
use App\Domain\Offers\Models\Offer;
use Ensi\TestFactories\FakerProvider;
use Tests\ElasticTestTrait;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class, ElasticTestTrait::class);
uses()->group('integration', 'OfferModel');

test("Offer delete success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */

    /** @var Offer $offer */
    $offer = Offer::factory()->create();
    OfferData::factory()->fromModel($offer)->create();

    $offer->delete();
    assertElasticMissing((new OfferIndex()), [
        'term' => [
            'offer_id' => $offer->offer_id,
        ],
    ]);
})->with(FakerProvider::$optionalDataset);
