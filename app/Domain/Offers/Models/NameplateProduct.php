<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\NameplateProductFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property int $nameplate_product_id - id связки тега и товара из CMS
 *
 * @property int $nameplate_id - id тега из CMS
 * @property int $product_id - id товара из PIM
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class NameplateProduct extends Model
{
    protected $table = 'nameplate_product';

    public static function factory(): NameplateProductFactory
    {
        return NameplateProductFactory::new();
    }
}
