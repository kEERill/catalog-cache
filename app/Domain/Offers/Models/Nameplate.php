<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\NameplateFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;

/**
 * @property int $id
 *
 * @property int $nameplate_id - id тега из CMS
 *
 * @property string $name - наименование тега
 * @property string $code - код тега
 *
 * @property string $background_color - цвет фона
 * @property string $text_color - цвет текста
 *
 * @property bool $is_active - активность
 *
 * @property CarbonInterface|null $created_at - дата создания
 * @property CarbonInterface|null $updated_at - дата обновления
 *
 * @property-read Collection|NameplateProduct[] $productLinks Привязки товаров к тегу
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class Nameplate extends Model
{
    protected $table = 'nameplates';

    public function productLinks(): HasMany
    {
        return $this->hasMany(NameplateProduct::class, 'nameplate_id', 'nameplate_id');
    }

    public static function factory(): NameplateFactory
    {
        return NameplateFactory::new();
    }
}
