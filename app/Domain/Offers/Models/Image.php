<?php

namespace App\Domain\Offers\Models;

use App\Domain\Offers\Models\Tests\Factories\ImageFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 *
 * @property int $image_id ID изображения из PIM
 *
 * @property int $product_id Id товара из PIM
 * @property string|null $name Описание картинки
 * @property int $sort Порядок сортировки
 * @property string|null $file Файл картинки
 *
 * @property CarbonInterface|null $created_at
 * @property CarbonInterface|null $updated_at
 *
 * @property bool $is_migrated - сохранено/создано при миграции записей из мастер-сервисов
 */
class Image extends Model
{
    public $table = 'images';

    public static function factory(): ImageFactory
    {
        return ImageFactory::new();
    }
}
