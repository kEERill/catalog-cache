<?php

namespace App\Domain\Support\Concerns;

use Illuminate\Database\Eloquent\Model;

trait MigrateEntities
{
    public function executeBeforeMigration(array $classes): void
    {
        /** @var Model $class */
        foreach ($classes as $class) {
            $class::query()->update(['is_migrated' => false]);
        }
    }

    public function executeAfterMigration(array $classes): void
    {
        /** @var Model $class */
        foreach ($classes as $class) {
            $class::query()->where('is_migrated', false)->delete();
        }
    }
}
