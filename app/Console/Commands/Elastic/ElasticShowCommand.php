<?php

namespace App\Console\Commands\Elastic;

use App\Domain\Elastic\Concerns\ElasticIndex;
use Illuminate\Console\Command;

class ElasticShowCommand extends Command
{
    protected $signature = 'elastic:show';
    protected $description = 'Показать информацию о рабочем индексе';

    public function handle(): void
    {
        foreach (config('elastic.indexes') as $indexClass => $settings) {
            /** @var ElasticIndex $index */
            $index = new $indexClass();

            $this->info("Index type: {$index->indexBaseName()}, Hash: {$index->settingsHash()}");
        }
    }
}
