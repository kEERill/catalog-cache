<?php

namespace App\Console\Commands\Elastic;

use App\Domain\Elastic\Actions\ApplyIndexAction;
use App\Domain\Elastic\Actions\ClearOldIndexAction;
use Illuminate\Console\Command;

class ElasticClearOldIndicesCommand extends Command
{
    protected $signature = 'elastic:clear-old-indices';
    protected $description = 'Удалить старые индексы';

    public function handle(ApplyIndexAction $applyIndexAction, ClearOldIndexAction $action): void
    {
        $applyIndexAction->execute(fn ($index) => $action->execute($index));
    }
}
