<?php

namespace App\Console\Commands\Elastic;

use App\Domain\Elastic\Actions\ApplyIndexAction;
use App\Domain\Elastic\Actions\MigrateIndexAction;
use Illuminate\Console\Command;

class ElasticIndexMigrateCommand extends Command
{
    protected $signature = 'elastic:index-migrate';
    protected $description = 'Проверить наличие актуального индекса и создать если его нет';

    public function handle(ApplyIndexAction $applyIndexAction, MigrateIndexAction $migrateIndexAction): void
    {
        $applyIndexAction->execute(fn ($index) => $migrateIndexAction->execute($index));
    }
}
