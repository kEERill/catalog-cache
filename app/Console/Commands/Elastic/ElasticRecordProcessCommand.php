<?php

namespace App\Console\Commands\Elastic;

use App\Domain\Elastic\Actions\ElasticRecordProcessAction;
use Illuminate\Console\Command;

class ElasticRecordProcessCommand extends Command
{
    protected $signature = 'elastic:records-process';
    protected $description = 'Добавить записи на индексацию в очередь';

    public function handle(ElasticRecordProcessAction $action): void
    {
        $action->execute();
    }
}
