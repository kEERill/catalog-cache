<?php

namespace App\Console\Commands\Offers;

use App\Domain\Offers\Actions\MigrateEntitiesFromCatalogAction;
use App\Domain\Offers\Actions\MigrateEntitiesFromCmsAction;
use Illuminate\Console\Command;

class OffersMigrateFromApiCommand extends Command
{
    protected $signature = 'offers:migrate-from-api';
    protected $description = 'Заполнить первично БД записями из мастер-сервисов';

    public function handle(
        MigrateEntitiesFromCatalogAction $catalogAction,
        MigrateEntitiesFromCmsAction $cmsAction,
    ): void {
        $catalogAction->execute();
        $cmsAction->execute();
    }
}
