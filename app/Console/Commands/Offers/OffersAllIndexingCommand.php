<?php

namespace App\Console\Commands\Offers;

use App\Domain\Offers\Actions\IndexAllOffersAction;
use Illuminate\Console\Command;

class OffersAllIndexingCommand extends Command
{
    protected $signature = 'offers:all-indexing';
    protected $description = 'Полностью переиндексировать офферы';

    public function handle(IndexAllOffersAction $indexOffers): int
    {
        $indexOffers->execute();

        return self::SUCCESS;
    }
}
