<?php

use App\Console\Commands\Elastic\ElasticIndexMigrateCommand;
use Ensi\LaravelElasticQuery\ElasticClient;

use function Pest\Laravel\artisan;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'commands', 'ElasticIndexMigrateCommand');

test("Command ElasticIndexMigrate exist success", function () {
    /** @var ComponentTestCase $this */

    $mock = $this->mock(ElasticClient::class);
    $mock->shouldReceive('indicesExists')->once()->andReturn(true);
    $mock->shouldNotReceive('indicesCreate');

    artisan(ElasticIndexMigrateCommand::class);
});

test("Command ElasticIndexMigrate create success", function () {
    /** @var ComponentTestCase $this */

    $mock = $this->mock(ElasticClient::class);
    $mock->shouldReceive('indicesExists')->once()->andReturn(false);
    $mock->shouldReceive('indicesCreate')->once();

    artisan(ElasticIndexMigrateCommand::class);
});
