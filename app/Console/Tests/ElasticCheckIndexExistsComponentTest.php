<?php

use App\Console\Commands\Elastic\ElasticCheckIndexExistsCommand;
use Ensi\LaravelElasticQuery\ElasticClient;

use function Pest\Laravel\artisan;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'commands', 'ElasticCheckIndexExistsCommand');

test("Command ElasticCheckIndexExists exist success", function () {
    /** @var ComponentTestCase $this */

    $mock = $this->mock(ElasticClient::class);
    $mock->shouldReceive('indicesExists')->andReturn(true);

    artisan(ElasticCheckIndexExistsCommand::class)->assertExitCode(0);
});

test("Command ElasticIndexMigrate exist error", function () {
    /** @var ComponentTestCase $this */

    $mock = $this->mock(ElasticClient::class);
    $mock->shouldReceive('indicesExists')->andReturn(false);

    artisan(ElasticCheckIndexExistsCommand::class)->assertExitCode(1);
});
