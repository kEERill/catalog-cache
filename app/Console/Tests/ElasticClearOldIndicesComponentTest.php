<?php

use App\Console\Commands\Elastic\ElasticClearOldIndicesCommand;
use App\Domain\Offers\Elastic\Tests\Factories\OfferIndexFactory;
use Ensi\LaravelElasticQuery\ElasticClient;

use function Pest\Laravel\artisan;
use function PHPUnit\Framework\assertEqualsCanonicalizing;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'commands', 'ElasticClearOldIndicesCommand');

test("Command ClearOldIndices without delete success", function (int $countIndices) {
    /** @var ComponentTestCase $this */

    $offerIndices = OfferIndexFactory::new()->makeSeveral($countIndices)->all();
    $mock = $this->mock(ElasticClient::class);

    $mock->shouldReceive('catIndices')->once()->andReturn($offerIndices);
    $mock->shouldNotReceive('indicesDelete');

    artisan(ElasticClearOldIndicesCommand::class);
})->with([
    2,
    1,
    0,
]);

test("Command ClearOldIndices with delete success", function () {
    /** @var ComponentTestCase $this */

    $date = now();
    $offersIndices = collect();
    for ($i = 0; $i <= 10; $i++) {
        $offersIndices->push(OfferIndexFactory::new()->make(['creation.date' => $date]));

        $date = $date->subDay();
    }

    $mock = $this->mock(ElasticClient::class);
    $mock->shouldReceive('catIndices')->andReturn($offersIndices->all());

    $offersIndices->shift(2);

    $deleteIndices = $offersIndices->pluck('index');

    $realDeleteIndices = [];
    $mock->shouldReceive('indicesDelete')->withArgs(function (string $index) use (&$realDeleteIndices) {
        $realDeleteIndices[] = $index;

        return true;
    });

    artisan(ElasticClearOldIndicesCommand::class);

    assertEqualsCanonicalizing($deleteIndices->all(), $realDeleteIndices);
});
