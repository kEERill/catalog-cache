<?php

namespace App\Http\ApiV1\Support\Pagination\Eloquent;

use App\Http\ApiV1\Support\Pagination\AbstractPageBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder as SpatieQueryBuilder;

abstract class AbstractEloquentPageBuilder extends AbstractPageBuilder
{
    public function __construct(protected Builder|SpatieQueryBuilder $query, protected Request $request)
    {
    }
}
