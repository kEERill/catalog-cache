<?php

namespace App\Http\ApiV1\Support\Pagination\Elastic;

use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Pagination\Page;
use Illuminate\Database\Eloquent\Collection;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use TypeError;
use UnexpectedValueException;

class CursorPageBuilder extends AbstractElasticPageBuilder
{
    public function build(): Page
    {
        $limit = $this->applyMaxLimit((int) $this->request->input('pagination.limit', $this->getDefaultLimit()));

        return $limit > 0
            ? $this->buildWithPositiveLimit($limit)
            : $this->buildWithNotPositiveLimit($limit);
    }

    protected function buildWithNotPositiveLimit(int $limit): Page
    {
        $hits = $limit < 0 && !$this->forbidToBypassPagination ? $this->query->get() : new Collection();

        return new Page($hits, [
            'cursor' => null,
            'limit' => $limit,
            'next_cursor' => null,
            'previous_cursor' => null,
            'type' => PaginationTypeEnum::CURSOR->value,
        ]);
    }

    protected function buildWithPositiveLimit(int $limit): Page
    {
        $cursorHash = $this->request->input('pagination.cursor', null);
        $cursorHash = $cursorHash === '' ? null : $cursorHash;

        try {
            $paginator = $this->query->cursorPaginate($limit, cursor: $cursorHash);
        } catch (TypeError|UnexpectedValueException $e) {
            throw new BadRequestHttpException("Invalid pagination cursor: {$e->getMessage()}");
        }

        return new Page($paginator->hits, [
            'cursor' => $paginator->current,
            'limit' => $limit,
            'next_cursor' => $paginator->next,
            'previous_cursor' => $paginator->previous,
            'type' => PaginationTypeEnum::CURSOR->value,
        ]);
    }
}
