<?php

namespace App\Http\ApiV1\Support\Pagination;

use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use Ensi\LaravelElasticQuerySpecification\SearchQueryBuilder;
use Illuminate\Database\Query\Builder;
use Illuminate\Http\Request;
use Spatie\QueryBuilder\QueryBuilder as SpatieQueryBuilder;

class PageBuilderFactory
{
    public function fromEloquentQuery(Builder|SpatieQueryBuilder $query, ?Request $request = null): AbstractPageBuilder
    {
        $request = $request ?: resolve(Request::class);

        return $this->isCursor($request)
            ? new Eloquent\CursorPageBuilder($query, $request)
            : new Eloquent\OffsetPageBuilder($query, $request);
    }

    public function fromElasticQuery(SearchQueryBuilder $query, ?Request $request = null): AbstractPageBuilder
    {
        $request = $request ?: resolve(Request::class);

        return $this->isCursor($request)
            ? new Elastic\CursorPageBuilder($query, $request)
            : new Elastic\OffsetPageBuilder($query, $request);
    }

    protected function isCursor($request): bool
    {
        return $request->input('pagination.type', config('pagination.default_type')) === PaginationTypeEnum::CURSOR->value;
    }
}
