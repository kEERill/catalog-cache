<?php

namespace App\Http\ApiV1\Support\Pagination;

abstract class AbstractPageBuilder
{
    protected bool $forbidToBypassPagination = false;

    protected ?int $maxLimit = null;

    abstract public function build(): Page;

    public function forbidToBypassPagination(bool $value = true): static
    {
        $this->forbidToBypassPagination = $value;

        return $this;
    }

    public function maxLimit(?int $maxLimit): static
    {
        $this->maxLimit = $maxLimit;

        return $this;
    }

    protected function applyMaxLimit(int $limit): int
    {
        return $limit = $this->maxLimit !== null && $this->maxLimit > 0 ? min($limit, $this->maxLimit) : $limit;
    }

    protected function getDefaultLimit(): int
    {
        return config('pagination.default_limit');
    }
}
