<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Elastic\Data\ImageData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin ImageData
 */
class ImagesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'sort' => $this->sort,
            'url' => $this->url,
        ];
    }
}
