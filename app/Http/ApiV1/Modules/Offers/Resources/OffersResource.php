<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Elastic\Data\OfferData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin OfferData
 */
class OffersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->offer_id,
            'product_id' => $this->product_id,
            'allow_publish' => $this->allow_publish,
            'main_image_file' => $this->mapPublicFileToResponse($this->main_image_file),
            'category_id' => $this->category_id,
            'brand_id' => $this->brand_id,
            'name' => $this->name,
            'code' => $this->code,
            'description' => $this->description,
            'type' => $this->type,
            'vendor_code' => $this->vendor_code,
            'barcode' => $this->barcode,
            'weight' => $this->weight,
            'weight_gross' => $this->weight_gross,
            'width' => $this->width,
            'height' => $this->height,
            'length' => $this->length,
            'is_adult' => $this->is_adult,
            'price' => $this->price,
            'cost' => $this->cost,
            'gluing_name' => $this->gluing_name,
            'gluing_is_main' => $this->gluing_name ? $this->gluing_is_main : null,
            'gluing_is_active' => $this->gluing_name ? $this->gluing_is_active : null,
            'discount' => DiscountResource::make($this->whenRequestInclude($request, 'discount', $this->discount)),
            'brand' => BrandsResource::make($this->whenRequestInclude($request, 'brand', $this->brand)),
            'category' => CategoriesResource::make($this->whenRequestInclude($request, 'category', $this->category)),
            'images' => ImagesResource::collection($this->whenRequestInclude($request, 'images', $this->images)),
            'attributes' => PropertiesResource::collection($this->whenRequestInclude($request, 'attributes', $this->props)),
            'gluing' => GluingResource::collection($this->whenRequestInclude($request, 'gluing', $this->gluing)),
            'nameplates' => NameplatesResource::collection($this->whenRequestInclude($request, 'nameplates', $this->nameplates)),
        ];
    }
}
