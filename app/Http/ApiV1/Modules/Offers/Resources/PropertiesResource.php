<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Elastic\Data\PropertyData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin PropertyData
 */
class PropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'type' => $this->type,
            'name' => $this->name,
            'code' => $this->code,
            'values' => PropertyValuesResource::collection($this->values),
        ];
    }
}
