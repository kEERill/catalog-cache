<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Elastic\Data\GluingPropertyData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin GluingPropertyData
 */
class GluingPropertiesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'prop_type' => $this->prop_type,
            'prop_name' => $this->prop_name,
            'prop_code' => $this->prop_code,
            'value_value' => $this->value_value,
            'value_name' => $this->value_name,
        ];
    }
}
