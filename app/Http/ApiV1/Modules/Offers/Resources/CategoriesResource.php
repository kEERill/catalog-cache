<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Elastic\Data\CategoryData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin CategoryData
 */
class CategoriesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'parent_id' => $this->parent_id,
        ];
    }
}
