<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Elastic\Data\DiscountData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin DiscountData
 */
class DiscountResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'value_type' => $this->value_type,
            'value' => $this->value,
        ];
    }
}
