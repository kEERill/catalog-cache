<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Elastic\Data\BrandData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin BrandData
 */
class BrandsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'description' => $this->description,
            'logo_file' => $this->mapPublicFileToResponse($this->logo_file),
            'logo_url' => $this->logo_url,
        ];
    }
}
