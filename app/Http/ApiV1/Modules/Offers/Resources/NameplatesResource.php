<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Elastic\Data\NameplateData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin NameplateData */
class NameplatesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'background_color' => $this->background_color,
            'text_color' => $this->text_color,
        ];
    }
}
