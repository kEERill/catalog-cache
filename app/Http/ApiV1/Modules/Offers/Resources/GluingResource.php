<?php

namespace App\Http\ApiV1\Modules\Offers\Resources;

use App\Domain\Offers\Elastic\Data\GluingData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin GluingData
 */
class GluingResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'props' => GluingPropertiesResource::collection($this->props),
        ];
    }
}
