<?php

use App\Domain\Offers\Elastic\Data\OfferData;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\TestFactories\FakerProvider;

use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/offers/offers:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $offers = [];
    for ($i = 0; $i < 10; $i++) {
        $offers[] = OfferData::factory()->withAllRelations()->create(['offer_id' => $i + 1]);
    }
    /** @var OfferData $offerData */
    $offerData = current($offers);

    postJson('/api/v1/offers/offers:search', [
        'include' => ['category', 'brand', 'images', 'attributes', 'gluing', 'discount', 'nameplates'], 'sort' => ['offer_id'],
    ])
        ->assertStatus(200)
        ->assertJsonCount(count($offers), 'data')
        ->assertJsonPath('data.0.id', $offerData->offer_id)
        ->assertJsonPath('data.0.discount.value', $offerData->discount->value)
        ->assertJsonPath('data.0.discount.value_type', $offerData->discount->value_type)
        ->assertJsonPath('data.0.brand.id', $offerData->brand->id)
        ->assertJsonPath('data.0.category.id', $offerData->category->id)
        ->assertJsonPath('data.0.images.0.id', current($offerData->images)->id)
        ->assertJsonPath('data.0.attributes.0.code', current($offerData->props)->code)
        ->assertJsonPath('data.0.gluing.0.id', current($offerData->gluing)->id)
        ->assertJsonPath('data.0.nameplates.0.id', current($offerData->nameplates)->id);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/offers/offers/{id} 200', function () {
    FakerProvider::$optionalAlways = true;

    /** @var OfferData $offerData */
    $offerData = OfferData::factory()->withAllRelations()->create();

    getJson("/api/v1/offers/offers/$offerData->offer_id?include=category,brand,images,attributes,gluing,discount,nameplates")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $offerData->offer_id)
        ->assertJsonPath('data.discount.value', $offerData->discount->value)
        ->assertJsonPath('data.discount.value_type', $offerData->discount->value_type)
        ->assertJsonPath('data.brand.id', $offerData->brand->id)
        ->assertJsonPath('data.category.id', $offerData->category->id)
        ->assertJsonPath('data.images.0.id', current($offerData->images)->id)
        ->assertJsonPath('data.attributes.0.code', current($offerData->props)->code)
        ->assertJsonPath('data.gluing.0.id', current($offerData->gluing)->id)
        ->assertJsonPath('data.nameplates.0.id', current($offerData->nameplates)->id);
})->with(FakerProvider::$optionalDataset);
