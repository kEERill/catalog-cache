<?php

namespace App\Http\ApiV1\Modules\Offers\Controllers;

use App\Domain\Offers\Elastic\Data\OfferData;
use App\Domain\Offers\Elastic\OfferIndex;
use App\Http\ApiV1\Modules\Offers\Queries\OffersQuery;
use App\Http\ApiV1\Modules\Offers\Resources\OffersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use Elastic\Elasticsearch\Exception\ClientResponseException;
use Illuminate\Contracts\Support\Responsable;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class OffersController
{
    public function search(OffersQuery $query, PageBuilderFactory $builderFactory): Responsable
    {
        $page = $builderFactory->fromElasticQuery($query)->build();

        $page->items = array_map(fn (array $item) => new OfferData($item['_source']), $page->items);

        return OffersResource::collectPage($page);
    }

    public function searchOne(OffersQuery $query, PageBuilderFactory $builderFactory): Responsable
    {
        $page = $builderFactory->fromElasticQuery($query)->build();
        if (!$page->items) {
            throw new NotFoundHttpException();
        }

        return OffersResource::make(new OfferData($page->items[0]['_source']));
    }

    public function get(int $id): Responsable
    {
        $index = new OfferIndex();

        try {
            $response = $index->get($id);
        } catch (ClientResponseException $e) {
            if ($e->getCode() === 404) {
                throw new NotFoundHttpException();
            }

            throw $e;
        }

        return OffersResource::make(new OfferData($response['_source']));
    }
}
