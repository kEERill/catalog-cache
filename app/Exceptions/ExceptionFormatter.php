<?php

namespace App\Exceptions;

use Throwable;

class ExceptionFormatter
{
    public static function line(string $msg, Throwable $e): string
    {
        return "{$msg}: {$e->getMessage()}. File: {$e->getFile()}. Line: {$e->getLine()}";
    }
}
