# Catalog Cache

## Резюме

Название: Catalog Cache  
Домен: Catalog  
Назначение: Сервис управления заказами

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

Регламент работы над задачами тоже находится в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/guid/regulations)

### Команды для работы с ElasticSearch

`elastic:show` - Показать информацию о рабочем индексе

`elastic:check-index-exists` - Проверить наличие актуального индекса

`elastic:index-migrate` - Проверить наличие актуального индекса и создать, если его нет

`elastic:records-process` - Проиндексировать записи, обновленные с момента последней индексации

`elastic:clear-old-indices` - Удалить старые индексы (храним 2 самых актуальных индекса)

#### Принятый порядок работы команд

1. При каждой отгрузке сервиса выполняется команда `elastic:index-migrate`, которая (при отсутствии индекса или изменении его структуры) создает индекс. 
2. Команда `elastic:records-process` запускается каждую минуту, и добавляет в очередь на индексацию сущности, изменившиеся с момента последнего запуска (это отслеживается через модель `IndexerTimestamp`. За счет того что время запуска фиксируется для уникальных hash индекса и APP_STAGE - можно запускать параллельно множество индексаторов).

Команда `elastic:clear-old-indices` запускается раз в день для удаления устаревших индексов.

Команда `elastic:check-index-exists` блокирует отгрузку web части сервиса, если индекс не существует.

#### Заполнение данных:

Основной поток заполнения данных идет через чтение топиков Kafka. Список топиков описан в блоке "Зависимости" - "Kafka". Для чтения данных топиков на стенде запущены воркеры, выполняющие команду `php artisan kafka:consume <имя топика>`.

Для первичного заполнения БД была реализована консольная команда `offers:migrate-from-api`, мигрирующая данные из мастер-сервисов с данными.

`offers:all-indexing` - Проиндексировать все доступные офферы

## Структура сервиса

Почитать про структуру сервиса можно здесь [здесь](docs/structure.md)

## Зависимости

| Название | Описание  | Переменные окружения |
| --- | --- | --- |
| PostgreSQL | Основная БД сервиса | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |
| Kafka | Брокер сообщений. <br/> Consumer слушает следующие топики:<br/> `<контур>.catalog.fact.offers.1`</br>`<контур>.catalog.fact.brands.1`</br>`<контур>.catalog.fact.categories.1`</br>`<контур>.catalog.fact.property-directory-values.1`</br>`<контур>.catalog.fact.properties.1`</br>`<контур>.catalog.fact.actual-category-properties.1`</br>`<контур>.catalog.fact.published-images.1`</br>`<контур>.catalog.fact.published-products.1`</br>`<контур>.catalog.fact.published-property-values.1`</br>`<контур>.catalog.fact.product-groups.1`</br>`<контур>.catalog.fact.product-group-products.1`</br> | KAFKA_CONTOUR<br/>KAFKA_BROKER_LIST<br/>KAFKA_SECURITY_PROTOCOL<br/>KAFKA_SASL_MECHANISMS<br/>KAFKA_SASL_USERNAME<br/>KAFKA_SASL_PASSWORD<br/> |


## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/catalog-cache/job/catalog-cache/  
URL: https://catalog-cache-master-dev.ensi.tech/docs/swagger

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
